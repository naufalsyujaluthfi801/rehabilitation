$(function() {
    $(".timepicker-input").datetimepicker({
        format: 'HH:mm',
        Default: moment.locale()
    });
});

function clickButtonAddFile() {
    let bodyUploadFile = $('#bodyUploadFile');
    let divRowUploadFile = bodyUploadFile.find('.row-upload-file').filter(':last');
    if (divRowUploadFile.length > 0) {
        let rowId = divRowUploadFile.attr('id');
        let rowIndexId = parseInt(rowId.replace(/\D/g, '')) + parseInt('1');
        let newDivRowHtml = '<div id="rowUploadFile' + rowIndexId + '" class="row row-upload-file">\n' +
            '   <div id="colUploadFile' + rowIndexId + '" class="col-9 col-upload-file">\n' +
            '       <input class="form-control" type="file" accept="image/*" id="files" name="files">\n' +
            '   </div>\n' +
            '   <div class="col-3 button-right-align">\n' +
            '       <button onclick="deleteButtonAddFile(this)" class="btn btn-danger" type="button">\n' +
            '           <svg class="icon me-2">\n' +
            '               <use xlink:href="/vendor/@coreui/icons/svg/free.svg#cil-trash"></use>\n' +
            '           </svg>Delete\n' +
            '       </button>\n' +
            '   </div>\n' +
            '</div>';
        bodyUploadFile.append(newDivRowHtml);
    }
}

function deleteButtonAddFile(elm) {
    $(elm).parent().parent().remove();
    reIndexRowAndColUploadFile();
}

function reIndexRowAndColUploadFile() {
    let bodyUploadFile = $('#bodyUploadFile');
    let divRowUploadFile = bodyUploadFile.find('.row-upload-file');
    if (divRowUploadFile.length > 0) {
        for (let i = 0; i < divRowUploadFile.length; i++) {
            let divColUploadFile = $(divRowUploadFile[i]).find('.col-upload-file');
            $(divRowUploadFile[i]).attr('id', 'rowUploadFile' + i);
            divColUploadFile.attr('id', 'colUploadFile' + i);
        }
    }
}