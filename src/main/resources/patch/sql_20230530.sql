CREATE TABLE "m_activity" (
                              "id" VARCHAR(50) NOT NULL,
                              "start_time" VARCHAR(50) NOT NULL,
                              "end_time" VARCHAR(50) NOT NULL,
                              "description_activity" TEXT NOT NULL,
                              "date" DATE NOT NULL,
                              "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                              "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                              "created_date" TIMESTAMP NULL DEFAULT NULL,
                              "created_by" VARCHAR(50) NULL DEFAULT NULL,
                              "updated_date" TIMESTAMP NULL DEFAULT NULL,
                              "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                              PRIMARY KEY ("id")
);

CREATE TABLE "m_upload" (
                            "id" VARCHAR(50) NOT NULL,
                            "filename" VARCHAR NOT NULL,
                            "filepath" VARCHAR NOT NULL,
                            "filesize" BIGINT NOT NULL,
                            "filetype" VARCHAR(100) NOT NULL,
                            "created_date" TIMESTAMP NULL DEFAULT NULL,
                            "created_by" VARCHAR(50) NULL DEFAULT NULL,
                            "updated_date" TIMESTAMP NULL DEFAULT NULL,
                            "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                            PRIMARY KEY ("id")
);