INSERT INTO m_permission (id, NAME, code, created_date, is_active, is_deleted)
VALUES
('0957c30f-b8b7-4bfc-b3ff-a8a28e9e3aba', 'Register patien sidebar', 'register-patient.sidebar.permission',NOW(), TRUE, FALSE);
('43085172-47cb-4bda-9bfb-06beea56dfc4', 'Activity sidebar', 'activity.sidebar.permission',NOW(), TRUE, FALSE);
('63d9cb08-547d-45eb-a54c-55980611da89', 'Activity input button', 'activity.input.button.permission',NOW(), TRUE, FALSE),
('c5998093-ab66-476b-9d64-4e12a2f14627', 'Activity view button', 'activity.view.button.permission',NOW(), TRUE, FALSE);


INSERT INTO m_role_permission (id, role_id, permission_id)
VALUES
('d5b789fa-3177-4e70-82e8-9a455e369bb7', '0957c30f-b8b7-4bfc-b3ff-a8a28e9e3aba', 'aa4c7d1b-0258-425c-b0fe-d9b6d0734b57'),
('30c3df23-5a00-47e0-b32e-bd3ee20ca7c1', '9b8cb0f1-2bd7-4fda-99ea-bc76e37a58ae', '43085172-47cb-4bda-9bfb-06beea56dfc4'),
('e90ab19c-8952-4932-9ce3-a465f978b259', '9e27c6e8-db8e-4718-a15d-96caa5732667', '43085172-47cb-4bda-9bfb-06beea56dfc4'),
('28ea75c0-f000-49df-8ba1-16a553c7af20', '211f8823-c039-4eff-a7f0-a803c1bfdabe', '43085172-47cb-4bda-9bfb-06beea56dfc4'),
('65647009-9ab1-4413-a356-0302e2b5d93c', '9e27c6e8-db8e-4718-a15d-96caa5732667', '63d9cb08-547d-45eb-a54c-55980611da89'),
('2ba4039b-57c6-42c4-a600-3d8314d934ab', '9e27c6e8-db8e-4718-a15d-96caa5732667', 'c5998093-ab66-476b-9d64-4e12a2f14627'),
('dd4e84cd-d839-48e9-8600-7bff50c00ea5', '9b8cb0f1-2bd7-4fda-99ea-bc76e37a58ae', 'c5998093-ab66-476b-9d64-4e12a2f14627'),
('e5974803-ec34-4b01-a4fe-ac1133e6b749', 'aa4c7d1b-0258-425c-b0fe-d9b6d0734b57', 'c5998093-ab66-476b-9d64-4e12a2f14627');