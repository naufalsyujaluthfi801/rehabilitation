CREATE TABLE "m_task_detail" (
                                 "id" VARCHAR(50) NOT NULL,
                                 "task_id" VARCHAR(50) NOT NULL,
                                 "upload_id" VARCHAR(50) NOT NULL,
                                 "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                                 "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                                 "created_date" TIMESTAMP NULL DEFAULT NULL,
                                 "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                 "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                 "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                                 PRIMARY KEY ("id"),
                                 CONSTRAINT "m_task_detail_task_id_fkey" FOREIGN KEY ("task_id") REFERENCES "public"."m_task" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
                                 CONSTRAINT "m_task_detail_upload_id_fkey" FOREIGN KEY ("upload_id") REFERENCES "public"."m_upload" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE m_task
DROP COLUMN uploadtask_id;