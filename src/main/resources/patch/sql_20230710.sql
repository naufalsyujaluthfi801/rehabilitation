INSERT INTO m_permission (id, NAME, code, created_date, is_active, is_deleted)
VALUES
('0e649cdc-7b0e-404b-b9bf-af89188ab5b2', 'Task register button permission', 'task.register.button.permission',NOW(), TRUE, FALSE),
('3a870587-97e7-43c2-8488-e1ffb143fb64', 'Task input button permission', 'task.input.button.permission',NOW(), TRUE, FALSE),
('340bebd7-4e44-474a-84c8-e48fc4659096', 'Task checked button permission', 'task.checked.button.permission',NOW(), TRUE, FALSE);

CREATE TABLE "t_material" (
                              "id" VARCHAR(50) NOT NULL,
                              "material_id" VARCHAR(50) NOT NULL,
                              "counselor_id" VARCHAR(50) NOT NULL,
                              "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                              "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                              "created_date" TIMESTAMP NULL DEFAULT NULL,
                              "created_by" VARCHAR(50) NULL DEFAULT NULL,
                              "updated_date"TIMESTAMP NULL DEFAULT NULL,
                              "updated_by"VARCHAR(50) NULL DEFAULT NULL,
                              PRIMARY KEY ("id"),
                              CONSTRAINT "t_material_material_id_fkey" FOREIGN KEY ("material_id") REFERENCES "public"."m_material" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);