INSERT INTO m_permission (id, NAME, code, created_date, is_active, is_deleted)
VALUES
('9714f2db-c447-4248-ba5b-d56ba4943f48', 'Dashboard sidebar', 'dashboard.sidebar.permission',NOW(), TRUE, FALSE);

INSERT INTO m_role_permission (id, role_id, permission_id)
VALUES
('cd7bdb44-f9d0-412f-b1cb-88bab19d4c24', '9b8cb0f1-2bd7-4fda-99ea-bc76e37a58ae', '9714f2db-c447-4248-ba5b-d56ba4943f48'),
('8d8419b6-c846-4651-b60e-d1db15a049e7', 'aa4c7d1b-0258-425c-b0fe-d9b6d0734b57', '9714f2db-c447-4248-ba5b-d56ba4943f48'),
('01231f7a-5815-49f9-a9cf-41bafdce15b8', '211f8823-c039-4eff-a7f0-a803c1bfdabe', '9714f2db-c447-4248-ba5b-d56ba4943f48'),
('3f27b349-d9d8-4aa4-9720-0a320ab16e3d', '9e27c6e8-db8e-4718-a15d-96caa5732667', '9714f2db-c447-4248-ba5b-d56ba4943f48');