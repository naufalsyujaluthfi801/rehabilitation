CREATE TABLE "m_material_detail" (
                                     "id" VARCHAR(50) NOT NULL,
                                     "material_id" VARCHAR(50) NOT NULL,
                                     "upload_id" VARCHAR(50) NOT NULL,
                                     "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                                     "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                                     "created_date" TIMESTAMP NULL DEFAULT NULL,
                                     "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                     "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                     "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                                     PRIMARY KEY ("id"),
                                     CONSTRAINT "m_material_detail_material_id_fkey" FOREIGN KEY ("material_id") REFERENCES "public"."m_material" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
                                     CONSTRAINT "m_material_detail_upload_id_fkey" FOREIGN KEY ("upload_id") REFERENCES "public"."m_upload" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE m_material
DROP COLUMN uploadmaterial_id;