ALTER TABLE m_user_detail
ADD COLUMN "address" TEXT NULL default null;

ALTER TABLE m_user_detail
ADD COLUMN "counselor_id" VARCHAR(50) NULL DEFAULT NULL;

ALTER TABLE m_user_detail
RENAME COLUMN no_ktp TO nik;

ALTER TABLE m_user_detail ALTER COLUMN nik DROP NOT NULL;