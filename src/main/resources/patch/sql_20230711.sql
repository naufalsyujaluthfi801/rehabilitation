CREATE TABLE "t_task" (
                          "id" VARCHAR(50) NOT NULL,
                          "task_id" VARCHAR(50) NOT NULL,
                          "counselor_id" VARCHAR(50) NOT NULL,
                          "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                          "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                          "created_date" TIMESTAMP NULL DEFAULT NULL,
                          "created_by" VARCHAR(50) NULL DEFAULT NULL,
                          "updated_date"TIMESTAMP NULL DEFAULT NULL,
                          "updated_by"VARCHAR(50) NULL DEFAULT NULL,
                          PRIMARY KEY ("id"),
                          CONSTRAINT "t_task_task_id_fkey" FOREIGN KEY ("task_id") REFERENCES "public"."m_task" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);