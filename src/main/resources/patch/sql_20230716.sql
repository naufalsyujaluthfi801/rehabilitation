INSERT INTO m_permission (id, NAME, code, created_date, is_active, is_deleted)
VALUES
('50f2f162-4540-44a6-85e5-03790fe4ec6f', 'Task deleted button permission', 'task.deleted.button.permission',NOW(), TRUE, FALSE);

INSERT INTO m_role_permission (id, role_id, permission_id)
VALUES
('65ba4bf4-40ae-4bd0-9ecd-dcd7d365a7b1', 'aa4c7d1b-0258-425c-b0fe-d9b6d0734b57', '50f2f162-4540-44a6-85e5-03790fe4ec6f');