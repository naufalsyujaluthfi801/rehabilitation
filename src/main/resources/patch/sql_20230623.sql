ALTER TABLE m_activity
DROP COLUMN upload_id;

CREATE TABLE "m_activity_detail" (
                                     "id" VARCHAR(50) NOT NULL,
                                     "activity_id" VARCHAR(50) NOT NULL,
                                     "upload_id" VARCHAR(50) NOT NULL,
                                     "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                                     "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                                     "created_date" TIMESTAMP NULL DEFAULT NULL,
                                     "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                     "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                     "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                                     PRIMARY KEY ("id"),
                                     CONSTRAINT "m_activity_detail_activity_id_fkey" FOREIGN KEY ("activity_id") REFERENCES "public"."m_activity" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
                                     CONSTRAINT "m_activity_detail_upload_id_fkey" FOREIGN KEY ("upload_id") REFERENCES "public"."m_upload" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);