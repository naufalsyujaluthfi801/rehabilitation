ALTER TABLE m_role
ADD COLUMN "type" VARCHAR (15) DEFAULT 'INTERNAL';

INSERT INTO m_role (id, NAME, code, TYPE , is_active , is_deleted, created_date)
VALUES
('9b8cb0f1-2bd7-4fda-99ea-bc76e37a58ae', 'Consultor', 'CONS','INTERNAL', TRUE, FALSE, NOW()),
('aa4c7d1b-0258-425c-b0fe-d9b6d0734b57', 'Consultor Assistant', 'ASSC','INTERNAL', TRUE, FALSE, NOW()),
('211f8823-c039-4eff-a7f0-a803c1bfdabe', 'Outpatient', 'OUTP', 'EXTERNAL', TRUE, FALSE, NOW()),
('9e27c6e8-db8e-4718-a15d-96caa5732667', 'Inpatient', 'INP', 'EXTERNAL', TRUE, FALSE, NOW());

SELECT * FROM m_role
WHERE TYPE IN ('INTERNAL')
