INSERT INTO m_permission (id, NAME, code, created_date, is_active, is_deleted)
VALUES
('9f608e31-c7fa-46ec-932c-d489891cbe28', 'Modify patien', 'modify-patient.permission',NOW(), TRUE, FALSE);
('9f0b89d9-fa45-4c3c-bcdc-c3a19882fac2', 'Material input button permission', 'Material.input.button.permission',NOW(), TRUE, FALSE);
('0fbf13c2-6458-48d1-9826-a847fec19bfb', 'Material view button permission', 'Material.view.button.permission',NOW(), TRUE, FALSE);


INSERT INTO m_role_permission (id, role_id, permission_id)
VALUES
('4a40351a-04a6-467e-8f48-05244569e6e1', '9e27c6e8-db8e-4718-a15d-96caa5732667', '9f608e31-c7fa-46ec-932c-d489891cbe28'),
('52b39741-a475-4643-85b2-9b62775ffac3', 'aa4c7d1b-0258-425c-b0fe-d9b6d0734b57', '9f0b89d9-fa45-4c3c-bcdc-c3a19882fac2'),
('1f2afa7a-c907-4b23-9199-606d234b2e5b', '9e27c6e8-db8e-4718-a15d-96caa5732667', '0fbf13c2-6458-48d1-9826-a847fec19bfb'),
('a78404c2-e2b0-4049-9a95-eaa86b260a8f', '9b8cb0f1-2bd7-4fda-99ea-bc76e37a58ae', '0fbf13c2-6458-48d1-9826-a847fec19bfb');