INSERT INTO m_permission (id, NAME, code, created_date, is_active, is_deleted)
VALUES
('ea068585-f8e5-4a28-98e4-da84666c18e8', 'Task answer table permission', 'task.answer.table.permission',NOW(), TRUE, FALSE);

INSERT INTO m_role_permission (id, role_id, permission_id)
VALUES
('5a0fac61-e943-488a-a981-b2220e4ae343', '9e27c6e8-db8e-4718-a15d-96caa5732667', 'ea068585-f8e5-4a28-98e4-da84666c18e8');