CREATE TABLE "m_material" (
                              "id" VARCHAR(50) NOT NULL,
                              "description_material" TEXT NOT NULL,
                              "date" DATE NOT NULL,
                              "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                              "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                              "created_date" TIMESTAMP NULL DEFAULT NULL,
                              "created_by" VARCHAR(50) NULL DEFAULT NULL,
                              "updated_date" TIMESTAMP NULL DEFAULT NULL,
                              "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                              PRIMARY KEY ("id")
);

CREATE TABLE "m_task" (
                          "id" VARCHAR(50) NOT NULL,
                          "description_task" TEXT NOT NULL,
                          "date" DATE NOT NULL,
                          "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                          "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                          "created_date" TIMESTAMP NULL DEFAULT NULL,
                          "created_by" VARCHAR(50) NULL DEFAULT NULL,
                          "updated_date" TIMESTAMP NULL DEFAULT NULL,
                          "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                          PRIMARY KEY ("id")
);