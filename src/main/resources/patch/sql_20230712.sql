CREATE TABLE "m_task_patient" (
                                  "id" VARCHAR(50) NOT NULL,
                                  "task_id" VARCHAR(50) NOT NULL,
                                  "description_task" TEXT NOT NULL,
                                  "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                                  "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                                  "created_date" TIMESTAMP NULL DEFAULT NULL,
                                  "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                  "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                  "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                                  PRIMARY KEY ("id"),
                                  CONSTRAINT "m_task_patient_id_fkey" FOREIGN KEY ("task_id") REFERENCES "public"."m_task" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "m_task_patient_detail" (
                                         "id" VARCHAR(50) NOT NULL,
                                         "task_patient_id" VARCHAR(50) NOT NULL,
                                         "upload_id" VARCHAR(50) NOT NULL,
                                         "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                                         "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                                         "created_date" TIMESTAMP NULL DEFAULT NULL,
                                         "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                         "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                         "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                                         PRIMARY KEY ("id"),
                                         CONSTRAINT "m_task_patient_detail_task_patient_id_fkey" FOREIGN KEY ("task_patient_id") REFERENCES "public"."m_task_patient" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
                                         CONSTRAINT "m_task_patient_detail_upload_id_fkey" FOREIGN KEY ("upload_id") REFERENCES "public"."m_upload" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);