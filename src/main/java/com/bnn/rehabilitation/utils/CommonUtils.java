package com.bnn.rehabilitation.utils;

import com.bnn.rehabilitation.constant.CommonConstant;
import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.dto.GenderDto;
import com.bnn.rehabilitation.dto.IsActiveDto;
import com.bnn.rehabilitation.enums.GenderEnum;
import com.bnn.rehabilitation.enums.IsActiveEnum;
import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommonUtils {
    public static List<IsActiveDto> getIsActiveOptions() {
        List<IsActiveDto> result = new ArrayList<>();
        List<IsActiveEnum> isActiveEnums = Arrays.asList(IsActiveEnum.TRUE, IsActiveEnum.FALSE);
        for (IsActiveEnum isActiveEnum: isActiveEnums) {
            String value;
            if (isActiveEnum.getKey())
                value = MessageUtils.getMessageString(MessageConstant.COMMON_PAGE_0013);
            else
                value = MessageUtils.getMessageString(MessageConstant.COMMON_PAGE_0014);
            IsActiveDto dto = new IsActiveDto(value, isActiveEnum.getKey());
            result.add(dto);
        }
        return result;
    }

    public static String getFilenameWithoutExtension(String filename) {
        return FilenameUtils.removeExtension(filename);
    }

    public static String getExtensionFromFilename(String filename) {
        return FilenameUtils.getExtension(filename);
    }

    public static List<GenderDto> getGenderOption() {
        List<GenderDto> result = new ArrayList<>();
        List<GenderEnum> genderEnums = Arrays.asList(GenderEnum.MALE, GenderEnum.FEMALE);
        for (GenderEnum genderEnum: genderEnums) {
            String value;
            if (genderEnum.getCode().equals(CommonConstant.MALE_CODE))
                value = MessageUtils.getMessageString(MessageConstant.COMMON_PAGE_0015);
            else
                value = MessageUtils.getMessageString(MessageConstant.COMMON_PAGE_0016);
            GenderDto dto = new GenderDto(genderEnum.getCode(), value);
            result.add(dto);
        }
        return result;
    }
}
