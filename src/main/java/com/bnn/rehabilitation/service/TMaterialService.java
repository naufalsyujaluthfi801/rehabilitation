package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.model.TMaterial;

import java.util.List;

public interface TMaterialService {
    TMaterial save(TMaterial tMaterial);
    List<TMaterial> findAllByCounselorId(String counselorId);
}
