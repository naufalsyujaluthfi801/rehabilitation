package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;

public interface MActivityService {
    RegisterActivityResponse insertActivity(RegisterActivityRequest request);
    ViewActivityResponse viewActivity(ViewActivityRequest request);
    ModifyActivityResponse modifyActivity(ModifyActivityRequest request);
    LoadModifyActivityResponse loadModify(LoadModifyActivityRequest request);
    DeleteActivityResponse deleteActivity(DeleteActivityRequest request);

}
