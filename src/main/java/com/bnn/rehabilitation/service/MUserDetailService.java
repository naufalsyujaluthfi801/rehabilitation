package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.model.MUserDetail;

import java.util.List;

public interface MUserDetailService {
    MUserDetail save(MUserDetail mUserDetail);
    MUserDetail findOneByUserId(String userId);
    List<MUserDetail> findAllByCounselorId(String counselorId);
}
