package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.TaskPatientDetailDto;
import com.bnn.rehabilitation.model.MTaskPatientDetail;

import java.util.List;

public interface MTaskPatientDetailService {
    MTaskPatientDetail save(MTaskPatientDetail mTaskPatientDetail);
    MTaskPatientDetail findOneById(String id);
    List<TaskPatientDetailDto> findAllByTaskPatientId(String taskId);

}
