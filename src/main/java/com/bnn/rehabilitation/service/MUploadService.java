package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.FileDto;
import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.model.MUpload;
import org.springframework.web.multipart.MultipartFile;

public interface MUploadService {
    MUpload insertUpload(MultipartFile file, UserSessionDto sessionDto);
    FileDto viewByUploadId(String uploadId);
}
