package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.model.TTask;

import java.util.List;

public interface TTaskService {
    TTask save (TTask tTask);
    List<TTask> findAllByTaskId(String taskId);
    List<TTask> findAllByCounselorId(String counselorId);
}
