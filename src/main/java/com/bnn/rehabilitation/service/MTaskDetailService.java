package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.MaterialDetailDto;
import com.bnn.rehabilitation.dto.TaskDetailDto;
import com.bnn.rehabilitation.model.MMaterialDetail;
import com.bnn.rehabilitation.model.MTaskDetail;

import java.util.List;

public interface MTaskDetailService {
    MTaskDetail save(MTaskDetail mTaskDetail);
    MTaskDetail findOneById(String id);
    List<TaskDetailDto> findAllByTaskId(String taskId);
    List<MTaskDetail> findByTaskId(String taskId);
}
