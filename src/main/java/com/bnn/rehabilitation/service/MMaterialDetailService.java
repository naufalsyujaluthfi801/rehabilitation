package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.ActivityDetailDto;
import com.bnn.rehabilitation.dto.MaterialDetailDto;
import com.bnn.rehabilitation.model.MActivityDetail;
import com.bnn.rehabilitation.model.MMaterialDetail;

import java.util.List;

public interface MMaterialDetailService {
    MMaterialDetail save(MMaterialDetail mMaterialDetail);
    MMaterialDetail findOneById(String id);
    List<MaterialDetailDto> findAllByMaterialId(String materialId);
    List<MMaterialDetail> findByMaterialId(String materialId);
}
