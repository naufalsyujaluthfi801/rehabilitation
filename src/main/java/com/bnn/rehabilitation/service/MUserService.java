package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;

public interface MUserService {
    SignupResponse insertUser(SignupRequest request);
    LoadSignupResponse loadSignup(LoadSignupRequest request);
    RegisterUserPatientResponse insertUserPatient(RegisterUserPatientRequest request);
    LoadRegisterUserPatientResponse loadUserPatient(LoadRegisterUserPatientRequest request);
    ModifyUserPatientResponse modifyUserPatient(ModifyUserPatientRequest request);
    LoadModifyUserPatientResponse loadModifyUserPatient(LoadModifyUserPatientRequest request);
    ViewPatientResponse viewPatient(ViewPatientRequest request);
    DeletePatientResponse deletePatient(DeletePatientRequest request);
}
