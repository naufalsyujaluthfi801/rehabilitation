package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.model.MRole;

import java.util.List;

public interface MRoleService {
    List <MRole> findAllByTypes(List<String> types);
}
