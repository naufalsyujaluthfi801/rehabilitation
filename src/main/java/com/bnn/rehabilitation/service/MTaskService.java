package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;

public interface MTaskService {
    RegisterTaskResponse insertTask(RegisterTaskRequest request);
    LoadRegisterTaskResponse loadTask(LoadRegisterTaskRequest request);
    ViewTaskResponse viewTask(ViewTaskRequest request);
    ModifyTaskPatientResponse modifyTask(ModifyTaskPatientRequest request);
    LoadModifyTaskPatientResponse loadModify(LoadModifyTaskPatientRequest request);
    DeleteTaskResponse deleteTask(DeleteTaskRequest request);
    ViewTaskPatientResponse viewTaskPatient(ViewTaskPatientRequest request);
}
