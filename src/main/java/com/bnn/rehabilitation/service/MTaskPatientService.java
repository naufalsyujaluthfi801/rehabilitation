package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.request.LoadModifyTaskPatientRequest;
import com.bnn.rehabilitation.dto.request.ModifyTaskPatientRequest;
import com.bnn.rehabilitation.dto.response.LoadModifyTaskPatientResponse;
import com.bnn.rehabilitation.dto.response.ModifyTaskPatientResponse;
import com.bnn.rehabilitation.model.MTaskPatient;

import java.util.List;

public interface MTaskPatientService {
    ModifyTaskPatientResponse modifyTaskPatient(ModifyTaskPatientRequest request);
    LoadModifyTaskPatientResponse loadModifyTaskPatient(LoadModifyTaskPatientRequest request);
    List<MTaskPatient>findAllByTaskIdAndUserId(String taskId, String userId);
    List<MTaskPatient>findAllByTaskId(String taskId);

}
