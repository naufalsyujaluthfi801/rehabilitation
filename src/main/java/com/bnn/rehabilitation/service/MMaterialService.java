package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.request.DeleteMaterialRequest;
import com.bnn.rehabilitation.dto.request.LoadRegisterMaterialRequest;
import com.bnn.rehabilitation.dto.request.RegisterMaterialRequest;
import com.bnn.rehabilitation.dto.request.ViewMaterialRequest;
import com.bnn.rehabilitation.dto.response.DeleteMaterialResponse;
import com.bnn.rehabilitation.dto.response.LoadRegisterMaterialResponse;
import com.bnn.rehabilitation.dto.response.RegisterMaterialResponse;
import com.bnn.rehabilitation.dto.response.ViewMaterialResponse;

public interface MMaterialService {
    RegisterMaterialResponse insertMaterial(RegisterMaterialRequest request);
    LoadRegisterMaterialResponse loadMaterial(LoadRegisterMaterialRequest request);
    ViewMaterialResponse viewMaterial(ViewMaterialRequest request);
    DeleteMaterialResponse deleteMaterial(DeleteMaterialRequest request);
}
