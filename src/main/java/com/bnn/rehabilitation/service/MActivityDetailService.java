package com.bnn.rehabilitation.service;

import com.bnn.rehabilitation.dto.ActivityDetailDto;
import com.bnn.rehabilitation.model.MActivityDetail;

import java.util.List;

public interface MActivityDetailService {
    MActivityDetail save(MActivityDetail mActivityDetail);
    MActivityDetail findOneById(String id);
    List<ActivityDetailDto> findAllByActivityId(String activityId);
    List<MActivityDetail> findByActivityId(String activityId);
}
