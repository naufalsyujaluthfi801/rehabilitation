package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.constant.CommonConstant;
import com.bnn.rehabilitation.constant.DateConstant;
import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.constant.ModuleSidebarConstant;
import com.bnn.rehabilitation.dto.MaterialDetailDto;
import com.bnn.rehabilitation.dto.MaterialDto;
import com.bnn.rehabilitation.dto.UserDto;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.model.*;
import com.bnn.rehabilitation.repository.MMaterialRepository;
import com.bnn.rehabilitation.repository.MRoleRepository;
import com.bnn.rehabilitation.repository.MUserRepository;
import com.bnn.rehabilitation.repository.TMaterialRepository;
import com.bnn.rehabilitation.service.*;
import com.bnn.rehabilitation.utils.DateUtils;
import com.bnn.rehabilitation.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@Transactional

public class MMaterialServiceImpl implements MMaterialService {
    private final MMaterialRepository mMaterialRepository;
    private final MUploadService mUploadService;
    private final MMaterialDetailService mMaterialDetailService;
    private final MRoleRepository mRoleRepository;
    private final MUserRepository mUserRepository;
    private final MUserDetailService mUserDetailService;
    private final TMaterialService tMaterialService;
    private final TMaterialRepository tMaterialRepository;

    public MMaterialServiceImpl(MMaterialRepository mMaterialRepository, MUploadService mUploadService, MMaterialDetailService mMaterialDetailService, MRoleRepository mRoleRepository, MUserRepository mUserRepository, MUserDetailService mUserDetailService, TMaterialService tMaterialService, TMaterialRepository tMaterialRepository) {
        this.mMaterialRepository = mMaterialRepository;
        this.mUploadService = mUploadService;
        this.mMaterialDetailService = mMaterialDetailService;
        this.mRoleRepository = mRoleRepository;
        this.mUserRepository = mUserRepository;
        this.mUserDetailService = mUserDetailService;
        this.tMaterialService = tMaterialService;
        this.tMaterialRepository = tMaterialRepository;
    }

    @Override
    public RegisterMaterialResponse insertMaterial(RegisterMaterialRequest request) {
        RegisterMaterialResponse response = new RegisterMaterialResponse();
        try {
            MMaterial mMaterial = new MMaterial();
            mMaterial.setId(UUID.randomUUID().toString());
            mMaterial.setDescriptionMaterial(request.getDescriptionMaterial());
            mMaterial.setDate(new Date());
            mMaterial.setIsActive(true);
            mMaterial.setIsDeleted(false);
            mMaterial.setCreatedDate(Instant.now());
            mMaterial.setCreatedBy(request.getUserSession().getUserId());
            this.mMaterialRepository.save(mMaterial);
            for(var index = 0; index<request.getFiles().length; index++){
                if (!request.getFiles()[index].isEmpty()) {
                    MUpload mUpload = mUploadService.insertUpload(request.getFiles()[index], request.getUserSession());
                    MMaterialDetail mMaterialDetail = new MMaterialDetail();
                    mMaterialDetail.setId(UUID.randomUUID().toString());
                    mMaterialDetail.setMaterial(mMaterial);
                    mMaterialDetail.setUpload(mUpload);
                    mMaterialDetail.setIsActive(true);
                    mMaterialDetail.setIsDeleted(false);
                    mMaterialDetail.setCreatedDate(Instant.now());
                    mMaterialDetail.setCreatedBy(request.getUserSession().getUserId());
                    this.mMaterialDetailService.save(mMaterialDetail);
                }
            }
            List<String> counselorIds = request.getCounselorIds();
            for (String counselorId:counselorIds) {
                TMaterial tMaterial = new TMaterial();
                tMaterial.setId(UUID.randomUUID().toString());
                tMaterial.setMaterial(mMaterial);
                tMaterial.setCounselorId(counselorId);
                tMaterial.setIsActive(true);
                tMaterial.setIsDeleted(false);
                tMaterial.setCreatedDate(Instant.now());
                tMaterial.setCreatedBy(request.getUserSession().getUserId());
                this.tMaterialService.save(tMaterial);
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0005)));
            log.info("Register Material Succesfull, result{}",mMaterial);

        }catch (Exception e){
            log.info("Register Material failed, Message{}", e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0005)));
        }
        return response;
    }

    @Override
    public LoadRegisterMaterialResponse loadMaterial(LoadRegisterMaterialRequest request) {
        LoadRegisterMaterialResponse response = new LoadRegisterMaterialResponse();
        try{
            List<UserDto> userDtoList = new ArrayList<>();
            MRole counselorRole = this.mRoleRepository.findOneByCode(CommonConstant.ROLE_CODE_COUNSELOR);
            List<MUser> users = this.mUserRepository.findAllByRoleId(counselorRole.getId());
            for (MUser mUser:users) {
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(mUser.getId());
                String fullname = mUser.getUsername();
                if (mUserDetail!=null && !mUserDetail.getFullName().isEmpty()){
                    fullname = mUserDetail.getFullName();
                }
                UserDto userDto = new UserDto();
                userDto.setId(mUser.getId());
                userDto.setUserName(mUser.getUsername());
                userDto.setFullName(fullname);
                userDtoList.add(userDto);
            }
            response.setIsSuccess(true);
            response.setUserDtoList(userDtoList);
            log.info("Load register Material successful");
        }catch (Exception e){
            log.error("Load register Material failed, error: {}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public ViewMaterialResponse viewMaterial(ViewMaterialRequest request) {
        ViewMaterialResponse viewMaterialResponse = new ViewMaterialResponse();
        try {
            Date date = null;
            if(request.getDate()==null){
                date = new Date();
            }else {
                date = DateUtils.stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004,request.getDate());
            }
            List<MMaterial> mMaterials = new ArrayList<>();
            if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_COUNSELOR)){
                List<TMaterial> tMaterials = this.tMaterialService.findAllByCounselorId(request.getUserSession().getUserId());
                List<String> materialIds = new ArrayList<>();
                for (TMaterial tMaterial:tMaterials) {
                    String materialId = tMaterial.getMaterial().getId();
                    materialIds.add(materialId);
                }
                mMaterials= this.mMaterialRepository.findAllByMaterialIds(materialIds, date);
            }else if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_INPATIENT) || request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_OUTPATIENT)){
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(request.getUserSession().getUserId());
                List<TMaterial> tMaterials = this.tMaterialService.findAllByCounselorId(mUserDetail.getCounselorId());
                List<String> materialIds = new ArrayList<>();
                for (TMaterial tMaterial:tMaterials) {
                    String materialId = tMaterial.getMaterial().getId();
                    materialIds.add(materialId);
                }
                mMaterials= this.mMaterialRepository.findAllByMaterialIds(materialIds, date);
            }else if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_ASSISTANT_COUNSELOR)){
                mMaterials = this.mMaterialRepository.findAllByIsActiveAndDeletedMaterial(date);
            }
            List<MaterialDto> materialDtos = new ArrayList<>();
            for (MMaterial material : mMaterials) {
                List <MaterialDetailDto> materialDetailDtos = this.mMaterialDetailService.findAllByMaterialId(material.getId());
                MaterialDto materialDto = new MaterialDto();
                materialDto.setId(material.getId());
                materialDto.setDescriptionMaterial(material.getDescriptionMaterial());
                materialDto.setMaterialDetailDtos(materialDetailDtos);
                materialDtos.add(materialDto);
            }
            viewMaterialResponse.setData(materialDtos);
            viewMaterialResponse.setIsSuccess(true);
            log.info("View Material Succesfull",mMaterials);
        }catch (Exception e){
            log.error("View Material Failed",e.getMessage());
            e.printStackTrace();
            viewMaterialResponse.setIsSuccess(false);
            viewMaterialResponse.setMessage(e.getMessage());
        }
        return viewMaterialResponse;
    }

    @Override
    public DeleteMaterialResponse deleteMaterial(DeleteMaterialRequest request) {
        DeleteMaterialResponse response = new DeleteMaterialResponse();
        try{
            MMaterial mMaterial = this.mMaterialRepository.findById(request.getId()).orElse(null);
            if(mMaterial == null)
                throw new Exception("Data m_material doesn't exists");
            mMaterial.setIsActive(false);
            mMaterial.setIsDeleted(true);
            mMaterial.setUpdatedDate(Instant.now());
            mMaterial.setUpdatedBy(request.getUserSession().getUserId());
            this.mMaterialRepository.save(mMaterial);
            List<MMaterialDetail> mMaterialDetails = this.mMaterialDetailService.findByMaterialId(mMaterial.getId());
            for (MMaterialDetail mMaterialDetail:mMaterialDetails) {
                mMaterialDetail.setIsActive(false);
                mMaterialDetail.setIsDeleted(true);
                mMaterialDetail.setUpdatedDate(Instant.now());
                mMaterialDetail.setUpdatedBy(request.getUserSession().getUserId());
                this.mMaterialDetailService.save(mMaterialDetail);
            }
            List<TMaterial> tMaterials = this.tMaterialRepository.findAllByMaterialId(mMaterial.getId());
            for (TMaterial tMaterial:tMaterials) {
                tMaterial.setIsActive(false);
                tMaterial.setIsDeleted(true);
                tMaterial.setUpdatedDate(Instant.now());
                tMaterial.setUpdatedBy(request.getUserSession().getUserId());
                this.tMaterialService.save(tMaterial);
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0003, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0005)));
            log.info("Delete material successful, result: {}", mMaterial);
        }catch (Exception e){
            e.printStackTrace();
            log.info("Delete material failed, message: {}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0006, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0005)));
        }
        return response;
    }

}
