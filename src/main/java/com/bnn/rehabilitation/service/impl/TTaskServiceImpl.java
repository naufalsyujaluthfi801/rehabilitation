package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.model.TTask;
import com.bnn.rehabilitation.repository.TTaskRepository;
import com.bnn.rehabilitation.service.TTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class TTaskServiceImpl implements TTaskService {

    private final TTaskRepository tTaskRepository;

    public TTaskServiceImpl(TTaskRepository tTaskRepository) {
        this.tTaskRepository = tTaskRepository;
    }

    @Override
    public TTask save(TTask tTask) {
        try {
            log.info("Save TTask Is Success");
            return this.tTaskRepository.save(tTask);
        }catch(Exception e){
            e.printStackTrace();
            log.error("Save TTask Failed,error:{}",e.getMessage());
            throw new RuntimeException("Save TTask Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<TTask> findAllByTaskId(String taskId) {
        try {
            List<TTask> tTasks = this.tTaskRepository.findAllByTaskId(taskId);
            log.info("Find All TTask By Task Id Is Success");
            return tTasks;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find All TTask By Task Id Failed,error:{}",e.getMessage());
            throw new RuntimeException("Find TTask By Task Id Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<TTask> findAllByCounselorId(String counselorId) {
        try{
            List<TTask> tTasks = this.tTaskRepository.findAllByCounselorId(counselorId);
            log.info("Find All t_material By Counselor Id Is Success");
            return tTasks;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find All t_material By Counselor Id Failed,error:{}",e.getMessage());
            throw new RuntimeException("Find t_material By Counselor Id Failed,error:" + e.getMessage());
        }
    }
}
