package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.model.MUserDetail;
import com.bnn.rehabilitation.repository.MUserDetailRepository;
import com.bnn.rehabilitation.service.MUserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class MUserDetailServiceImpl implements MUserDetailService {

    private final MUserDetailRepository mUserDetailRepository;

    public MUserDetailServiceImpl(MUserDetailRepository mUserDetailRepository) {
        this.mUserDetailRepository = mUserDetailRepository;
    }

    @Override
    public MUserDetail save(MUserDetail mUserDetail) {
        try{
            log.info("Save User Detail Is Success");
            return this.mUserDetailRepository.save(mUserDetail);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Save User Detail Failed,error:{}",e.getMessage());
            throw new RuntimeException("Save User Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public MUserDetail findOneByUserId(String userId) {
        try{
            MUserDetail mUserDetail = this.mUserDetailRepository.findOneByUserId(userId);
            log.info("Find One m_user_detail By User Id Is Success");
            return mUserDetail;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find One m_user_detail By User Id Failed,error:{}",e.getMessage());
            throw new RuntimeException("Find One m_user_detail By User Id Failed,error:" + e.getMessage());
        }

    }

    @Override
    public List<MUserDetail> findAllByCounselorId(String counselorId) {
        try{
            List<MUserDetail> mUserDetails = this.mUserDetailRepository.findAllByCounselorId(counselorId);
            log.info("Find All m_user_detail By Counselor Id Is Success");
            return mUserDetails;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find All m_user_detail By Counselor Id Failed,error:{}",e.getMessage());
            throw new RuntimeException("Find All m_user_detail By Counselor Id Failed,error:" + e.getMessage());
        }
    }
}
