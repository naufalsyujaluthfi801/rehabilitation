package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.constant.ModuleSidebarConstant;
import com.bnn.rehabilitation.dto.*;
import com.bnn.rehabilitation.dto.request.LoadModifyTaskPatientRequest;
import com.bnn.rehabilitation.dto.request.ModifyTaskPatientRequest;
import com.bnn.rehabilitation.dto.response.LoadModifyTaskPatientResponse;
import com.bnn.rehabilitation.dto.response.ModifyTaskPatientResponse;
import com.bnn.rehabilitation.model.*;
import com.bnn.rehabilitation.repository.MTaskPatientRepository;
import com.bnn.rehabilitation.repository.MTaskRepository;
import com.bnn.rehabilitation.service.MTaskPatientDetailService;
import com.bnn.rehabilitation.service.MTaskDetailService;
import com.bnn.rehabilitation.service.MTaskPatientService;
import com.bnn.rehabilitation.service.MUploadService;
import com.bnn.rehabilitation.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@Transactional
public class MTaskPatientServiceImpl implements MTaskPatientService {

    private final MTaskRepository mTaskRepository;
    private final MUploadService mUploadService;
    private final MTaskPatientDetailService mTaskPatientDetailService;
    private final MTaskDetailService mTaskDetailService;
    private final MTaskPatientRepository mTaskPatientRepository;

    public MTaskPatientServiceImpl(MTaskRepository mTaskRepository, MUploadService mUploadService, MTaskPatientDetailService mTaskPatientDetailService, MTaskDetailService mTaskDetailService, MTaskPatientRepository mTaskPatientRepository) {
        this.mTaskRepository = mTaskRepository;
        this.mUploadService = mUploadService;
        this.mTaskPatientDetailService = mTaskPatientDetailService;
        this.mTaskDetailService = mTaskDetailService;
        this.mTaskPatientRepository = mTaskPatientRepository;
    }

    @Override
    public ModifyTaskPatientResponse modifyTaskPatient(ModifyTaskPatientRequest request) {
        ModifyTaskPatientResponse response = new ModifyTaskPatientResponse();
        try {
            MTask mTask = this.mTaskRepository.findById(request.getId()).orElse(null);
            if (mTask == null)
                throw new Exception("Data m_task doesn't exists");
            MTaskPatient mTaskPatient = this.mTaskPatientRepository.findOneByTaskIdAndUserId(mTask.getId(), request.getUserSession().getUserId());
            if (mTaskPatient == null){
                mTaskPatient = new MTaskPatient();
                mTaskPatient.setId(UUID.randomUUID().toString());
                mTaskPatient.setTask(mTask);
                mTaskPatient.setDescriptionTask(request.getDescriptionTask());
                mTaskPatient.setIsActive(true);
                mTaskPatient.setIsDeleted(false);
                mTaskPatient.setCreatedDate(Instant.now());
                mTaskPatient.setCreatedBy(request.getUserSession().getUserId());
                this.mTaskPatientRepository.save(mTaskPatient);
            }
            // Buat Proces insert File Baru//
            for (var index = 0; index<request.getFiles().length; index++){
                if (!request.getFiles()[index].isEmpty()){
                    MUpload mUpload = this.mUploadService.insertUpload(request.getFiles()[index], request.getUserSession());
                    MTaskPatientDetail mTaskPatientDetail = new MTaskPatientDetail();
                    mTaskPatientDetail.setId(UUID.randomUUID().toString());
                    mTaskPatientDetail.setTaskPatient(mTaskPatient);
                    mTaskPatientDetail.setUpload(mUpload);
                    mTaskPatientDetail.setIsActive(true);
                    mTaskPatientDetail.setIsDeleted(false);
                    mTaskPatientDetail.setCreatedDate(Instant.now());
                    mTaskPatientDetail.setCreatedBy(request.getUserSession().getUserId());
                    this.mTaskPatientDetailService.save(mTaskPatientDetail);
                }
            }
            // Buat Delete File Data yang sudah ada//
            if (request.getTaskPatientDetailDtos()!= null && !request.getTaskPatientDetailDtos().isEmpty()){
                for (TaskPatientDetailDto taskPatientDetailDto:request.getTaskPatientDetailDtos()) {
                    if (taskPatientDetailDto.getIsDeleted() == true){
                        MTaskPatientDetail mTaskPatientDetail = this.mTaskPatientDetailService.findOneById(taskPatientDetailDto.getId());
                        if (mTaskPatientDetail != null){
                            mTaskPatientDetail.setIsActive(false);
                            mTaskPatientDetail.setIsDeleted(true);
                            mTaskPatientDetail.setUpdatedBy(request.getUserSession().getUserId());
                            mTaskPatientDetail.setUpdatedDate(Instant.now());
                            this.mTaskPatientDetailService.save(mTaskPatientDetail);
                        }
                    }
                }
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0002, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0007)));
            log.info("Modify Task Patient successful, result: {}",mTask);
        }catch (Exception e){
            e.printStackTrace();
            log.info("Modify Task Patient failed, message :{}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0005, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0007)));
        }
        return response;
    }

    @Override
    public LoadModifyTaskPatientResponse loadModifyTaskPatient(LoadModifyTaskPatientRequest request) {
        LoadModifyTaskPatientResponse response = new LoadModifyTaskPatientResponse();
        try {
            MTask mTask = this.mTaskRepository.findById(request.getId()).orElse(null);
            if(mTask == null)
                throw new Exception("Data task doesn't exist with id: " + request.getId());
            MTaskPatient mTaskPatient = this.mTaskPatientRepository.findOneByTaskIdAndUserId(mTask.getId(), request.getUserSession().getUserId());
            TaskPatientDto taskPatientDto = new TaskPatientDto();
            if (mTaskPatient != null){
                List<TaskPatientDetailDto> taskPatientDetailDtos = this.mTaskPatientDetailService.findAllByTaskPatientId(mTaskPatient.getId());
                taskPatientDto.setDescriptionTask(mTaskPatient.getDescriptionTask());
                taskPatientDto.setTaskPatientDetailDtos(taskPatientDetailDtos);
            }
            taskPatientDto.setId(mTask.getId());
            response.setData(taskPatientDto);
            response.setIsSuccess(true);
        }catch (Exception e){
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public List<MTaskPatient> findAllByTaskIdAndUserId(String taskId, String userId) {
        try {
            log.info("Find Task Patient By Task Id And User Id is Success");
            return this.mTaskPatientRepository.findAllByTaskIdAndUserId(taskId, userId);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find Task Patient By Task Id And User Id Failed:{}" + e.getMessage());
            throw new RuntimeException("Task Patient By Task Id And User Id Failed:" + e.getMessage());
        }
    }

    @Override
    public List<MTaskPatient> findAllByTaskId(String taskId) {
        try {
            log.info("Find Task Patient By Task Id is Success");
            return this.mTaskPatientRepository.findAllByTaskId(taskId);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find Task Patient By Task Id Failed:{}" + e.getMessage());
            throw new RuntimeException("Task Patient By Task Id Failed:" + e.getMessage());
        }
    }
}
