package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.model.MRole;
import com.bnn.rehabilitation.repository.MRoleRepository;
import com.bnn.rehabilitation.service.MRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class MRoleServiceImpl implements MRoleService {

    private final MRoleRepository mRoleRepository;

    public MRoleServiceImpl(MRoleRepository mRoleRepository) {
        this.mRoleRepository = mRoleRepository;
    }


    @Override
    public List<MRole> findAllByTypes(List<String> types) {
        return this.mRoleRepository.findAllByTypes(types);
    }
}
