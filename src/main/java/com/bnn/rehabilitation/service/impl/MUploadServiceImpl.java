package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.constant.DateConstant;
import com.bnn.rehabilitation.dto.FileDto;
import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.model.MUpload;
import com.bnn.rehabilitation.repository.MUploadRepository;
import com.bnn.rehabilitation.service.MUploadService;
import com.bnn.rehabilitation.utils.CommonUtils;
import com.bnn.rehabilitation.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.JoinPointSignature;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.management.RuntimeErrorException;
import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Service
@Transactional
public class MUploadServiceImpl implements MUploadService {

    private final MUploadRepository mUploadRepository;
    private Path uploadFilePath;

    public MUploadServiceImpl(MUploadRepository mUploadRepository, Environment environment) {
        this.mUploadRepository = mUploadRepository;
        this.uploadFilePath = Paths.get(environment.getProperty("upload.file.path","/bnn/rehabilitation/uploads"));
        try {
            Files.createDirectories(uploadFilePath);
        }catch (Exception e){
            log.error("Folder gagal dibuat");
        }
    }

    @Override
    public MUpload insertUpload(MultipartFile file, UserSessionDto sessionDto) {
        try {
            if(file != null && !file.isEmpty()){
                String filename = CommonUtils.getFilenameWithoutExtension(file.getOriginalFilename());
                String extension = CommonUtils.getExtensionFromFilename(file.getOriginalFilename());
                String finalFilename = filename +"_"+DateUtils.dateToStringUsingSimpleDateFormat(DateConstant.DATE_FORMAT_0012,new Date())+"."+extension;
                Path targetLocation = this.uploadFilePath.resolve(finalFilename);
                MUpload mUpload = new MUpload();
                mUpload.setId(UUID.randomUUID().toString());
                mUpload.setFilename(finalFilename);
                mUpload.setFilepath(targetLocation.toFile().getPath());
                mUpload.setFilesize(file.getSize());
                mUpload.setFiletype(file.getContentType());
                mUpload.setCreatedDate(Instant.now());
                mUpload.setCreatedBy(sessionDto.getUserId());
                this.mUploadRepository.save(mUpload);
                Files.copy(file.getInputStream(),targetLocation);
                return mUpload;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public FileDto viewByUploadId(String uploadId) {
        try {
            MUpload mUpload = this.mUploadRepository.findById(uploadId).orElseThrow(()->new RuntimeException("MUpload Not Available"));
            File file = new File(mUpload.getFilepath());
            if (file.exists()||file.isFile()){
                FileDto fileDto = new FileDto();
                fileDto.setMimeType(mUpload.getFiletype());
                fileDto.setFilename(mUpload.getFilename());
                fileDto.setFileId(mUpload.getId());
                fileDto.setFile(file);
                return fileDto;
            }else{
                throw new RuntimeException("File doesn't exist with id:"+uploadId);
            }

        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("View Failed");
        }
    }
}
