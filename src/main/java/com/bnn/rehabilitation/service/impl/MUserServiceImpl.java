package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.constant.CommonConstant;
import com.bnn.rehabilitation.constant.DateConstant;
import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.constant.ModuleSidebarConstant;
import com.bnn.rehabilitation.dto.*;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.model.MRole;
import com.bnn.rehabilitation.model.MUser;
import com.bnn.rehabilitation.model.MUserDetail;
import com.bnn.rehabilitation.repository.MRoleRepository;
import com.bnn.rehabilitation.repository.MUserRepository;
import com.bnn.rehabilitation.service.MUserDetailService;
import com.bnn.rehabilitation.service.MUserService;
import com.bnn.rehabilitation.utils.CommonUtils;
import com.bnn.rehabilitation.utils.DateUtils;
import com.bnn.rehabilitation.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;

@Slf4j
@Service
@Transactional
public class MUserServiceImpl implements MUserService {

    private MUserRepository mUserRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private MRoleRepository mRoleRepository;
    private MUserDetailService mUserDetailService;

    @Autowired
    public MUserServiceImpl(MUserRepository mUserRepository, BCryptPasswordEncoder passwordEncoder, MRoleRepository mRoleRepository, MUserDetailService mUserDetailService) {
        this.mUserRepository = mUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.mRoleRepository = mRoleRepository;
        this.mUserDetailService = mUserDetailService;
    }

    @Override
    public SignupResponse insertUser(SignupRequest request) {
        SignupResponse response = new SignupResponse();
        try {
            boolean isExists = this.mUserRepository.existsMUserByUsername(request.getUsername());
            if (!isExists) {
                String password = passwordEncoder.encode(request.getPassword()).toString();
                MRole mRole = this.mRoleRepository.findById(request.getRoleId()).orElse(null);
                if (mRole == null){
                    throw new RuntimeException("Role Id Is Null");
                }
                MUser mUser = new MUser(request, password, mRole);
                this.mUserRepository.save(mUser);
                MUserDetail mUserDetail = new MUserDetail();
                mUserDetail.setId(UUID.randomUUID().toString());
                mUserDetail.setFullName(request.getFullName());
                mUserDetail.setUser(mUser);
                mUserDetail.setCreatedDate(Instant.now());
                mUserDetail.setCreatedBy(mUser.getId());
                this.mUserDetailService.save(mUserDetail);
                response.setIsSuccess(true);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
            } else {
                response.setIsSuccess(false);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
            }
        } catch (Exception e) {
            log.error(CommonConstant.EXCEPTION, e);
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
        }
        return response;
    }

    @Override
    public LoadSignupResponse loadSignup(LoadSignupRequest request) {
        LoadSignupResponse loadSignupResponse = new LoadSignupResponse();
        try {
            List <String> types = Arrays.asList(CommonConstant.ROLE_INTERNAL_TYPE);
            List <MRole> mRoles = this.mRoleRepository.findAllByTypes(types);
            List <RoleDto> roleDtos = new ArrayList<>();
            for (MRole role:mRoles) {
                RoleDto dto = new RoleDto();
                dto.setCode(role.getCode());
                dto.setId(role.getId());
                dto.setIsActive(role.getIsActive());
                dto.setName(role.getName());
                dto.setType(role.getType());
                roleDtos.add(dto);
            }
            loadSignupResponse.setRoleDtolist(roleDtos);
            loadSignupResponse.setIsSuccess(true);
        }catch (Exception e) {
            log.error(CommonConstant.EXCEPTION, e);
            loadSignupResponse.setIsSuccess(false);
            loadSignupResponse.setMessage(e.getMessage());
        }
        return loadSignupResponse;
    }

    @Override
    public RegisterUserPatientResponse insertUserPatient(RegisterUserPatientRequest request) {
        RegisterUserPatientResponse response = new RegisterUserPatientResponse();
        try {
            boolean isExists = this.mUserRepository.existsMUserByUsername(request.getUserName());
            if (!isExists){
                String password = this.passwordEncoder.encode(request.getPassword().toString());
                MUser mUser = new MUser();
                MRole mRole = this.mRoleRepository.findById(request.getRoleId()).orElse(null);
                if(mRole == null){
                    throw new Exception("Data m_role doesn't exist");
                }
                mUser.setId(UUID.randomUUID().toString());
                mUser.setUsername(request.getUserName());
                mUser.setPassword(password);
                mUser.setRole(mRole);
                mUser.setIsActive(true);
                mUser.setIsDeleted(false);
                mUser.setCreatedDate(Instant.now());
                mUser.setCreatedBy(request.getUserSession().getUserId());
                this.mUserRepository.save(mUser);
                MUserDetail mUserDetail = new MUserDetail();
                mUserDetail.setId(UUID.randomUUID().toString());
                mUserDetail.setNik(request.getNik());
                mUserDetail.setFullName(request.getFullName());
                mUserDetail.setDateOfBirth(DateUtils.stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, request.getDateOfBirth()));
                mUserDetail.setPlaceOfBirth(request.getPlaceOfBirth());
                mUserDetail.setAddress(request.getAddress());
                mUserDetail.setGender(request.getGender());
                mUserDetail.setUser(mUser);
                mUserDetail.setCounselorId(request.getCounselorId());
                mUserDetail.setIsActive(true);
                mUserDetail.setIsDeleted(false);
                mUserDetail.setCreatedDate(Instant.now());
                mUserDetail.setCreatedBy(request.getUserSession().getUserId());
                this.mUserDetailService.save(mUserDetail);
                response.setIsSuccess(true);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0008)));
                log.info("Register Account Patient Succesfull, result{}",mUser);
            }
        }catch (Exception e){
            log.info("Register Account Patient Failed, Message{}",e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0008)));
        }
        return response;
    }

    @Override
    public LoadRegisterUserPatientResponse loadUserPatient(LoadRegisterUserPatientRequest request) {
        LoadRegisterUserPatientResponse response = new LoadRegisterUserPatientResponse();
        try{
            List<String> types =Arrays.asList(CommonConstant.ROLE_EXTERNAL_TYPE);
            List<MRole> externalRoles = this.mRoleRepository.findAllByTypes(types);
            List<GenderDto> genderDtoList = CommonUtils.getGenderOption();
            List<RoleDto> roleDtos = new ArrayList<>();
            List<UserDto> userDtoList = new ArrayList<>();
            MRole counselorRole = this.mRoleRepository.findOneByCode(CommonConstant.ROLE_CODE_COUNSELOR);
            List<MUser> users = this.mUserRepository.findAllByRoleId(counselorRole.getId());
            for (MUser mUser:users) {
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(mUser.getId());
                String fullname = mUser.getUsername();
                if (mUserDetail!=null && !mUserDetail.getFullName().isEmpty()){
                    fullname = mUserDetail.getFullName();
                }
                UserDto userDto = new UserDto();
                userDto.setId(mUser.getId());
                userDto.setUserName(mUser.getUsername());
                userDto.setFullName(fullname);
                userDtoList.add(userDto);
            }
            for (MRole role:externalRoles) {
                RoleDto dto = new RoleDto();
                dto.setCode(role.getCode());
                dto.setId(role.getId());
                dto.setIsActive(role.getIsActive());
                dto.setName(role.getName());
                dto.setType(role.getType());
                roleDtos.add(dto);
            }
            response.setIsSuccess(true);
            response.setRoleDtoList(roleDtos);
            response.setGenderDtoList(genderDtoList);
            response.setUserDtoList(userDtoList);
            log.info("Load register User Patient successful");
        }catch (Exception e){
            log.error("Load register User Patient failed, error: {}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public ModifyUserPatientResponse modifyUserPatient(ModifyUserPatientRequest request) {
        ModifyUserPatientResponse response = new ModifyUserPatientResponse();
        try {
            boolean isExists = this.mUserRepository.existsMUserByUsername(request.getNewUsername());
            if (!isExists){
                MUser mUser = this.mUserRepository.findById(request.getId()).orElse(null);
                if(mUser == null){
                    throw new RuntimeException("Data m_user doesn't exists");
                }
                if(request.getPassword()!=null && !request.getPassword().isEmpty()){
                    String password = this.passwordEncoder.encode(request.getPassword().toString());
                    mUser.setPassword(password);
                }
                if(request.getNewUsername()!=null && !request.getNewUsername().isEmpty()){
                    mUser.setUsername(request.getNewUsername());
                }
                mUser.setUpdatedDate(Instant.now());
                mUser.setUpdatedBy(request.getUserSession().getUserId());
                this.mUserRepository.save(mUser);
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(request.getId());
                if(mUserDetail == null){
                    throw new RuntimeException("Data m_user_detail doesn't exists");
                }
                if(request.getNik()!=null && !request.getNik().isEmpty()){
                    mUserDetail.setNik(request.getNik());
                }
                if(request.getFullName()!=null && !request.getFullName().isEmpty()){
                    mUserDetail.setFullName(request.getFullName());
                }
                if(request.getDateOfBirth()!=null && !request.getDateOfBirth().isEmpty()){
                    mUserDetail.setDateOfBirth(DateUtils.stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, request.getDateOfBirth()));
                }
                if(request.getPlaceOfBirth()!=null && !request.getPlaceOfBirth().isEmpty()){
                    mUserDetail.setPlaceOfBirth(request.getPlaceOfBirth());
                }
                if(request.getAddress()!=null && !request.getAddress().isEmpty()){
                    mUserDetail.setAddress(request.getAddress());
                }
                if(request.getGender()!=null && !request.getGender().isEmpty()){
                    mUserDetail.setGender(request.getGender());
                }
                mUserDetail.setUpdatedDate(Instant.now());
                mUserDetail.setUpdatedBy(request.getUserSession().getUserId());
                this.mUserDetailService.save(mUserDetail);
                response.setIsSuccess(true);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0008)));
                log.info("Modify Account Patient Succesfull, result{}",mUser);
            }
        }catch (Exception e){
            log.info("Modify Account Patient Failed, Message{}",e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0008)));
        }
        return response;
    }

    @Override
    public LoadModifyUserPatientResponse loadModifyUserPatient(LoadModifyUserPatientRequest request) {
        LoadModifyUserPatientResponse response = new LoadModifyUserPatientResponse();
        try {
            if (request.getUserSessionDto() == null){
                throw new RuntimeException("Data User Session doesn't exists");
            }
            MUser mUser = this.mUserRepository.findById(request.getUserSessionDto().getUserId()).orElse(null);
            MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(request.getUserSessionDto().getUserId());
            List<GenderDto> genderDtoList = CommonUtils.getGenderOption();
            if (mUser == null){
                throw new RuntimeException("Data user doesn't exists with id" + request.getUserSessionDto().getUserId());
            }
            UserDetailDto userDetailDto = new UserDetailDto();
            userDetailDto.setId(mUser.getId());
            userDetailDto.setNik(mUserDetail.getNik());
            userDetailDto.setFullName(mUserDetail.getFullName());
            userDetailDto.setUserName(mUser.getUsername());
            userDetailDto.setPassword(mUser.getPassword());
            userDetailDto.setDateOfBirth(DateUtils.dateToStringUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, mUserDetail.getDateOfBirth()));
            userDetailDto.setPlaceOfBirth(mUserDetail.getPlaceOfBirth());
            userDetailDto.setAddress(mUserDetail.getAddress());
            userDetailDto.setGender(mUserDetail.getGender());
            response.setData(userDetailDto);
            response.setIsSuccess(true);
            response.setGenderDtoList(genderDtoList);
        }catch (Exception e){
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public ViewPatientResponse viewPatient(ViewPatientRequest request) {
        ViewPatientResponse response = new ViewPatientResponse();
        try {
            List<String> types = Arrays.asList(CommonConstant.ROLE_EXTERNAL_TYPE);
            List<MRole> internalRoles = this.mRoleRepository.findAllByTypes(types);
            List<String> roleIds = new ArrayList<>();
            for (MRole mRole:internalRoles) {
                String roleId = mRole.getId();
                roleIds.add(roleId);
            }
            List<MUser> users = this.mUserRepository.findAllByRoleIds(roleIds);
            List<UserDetailDto> userDetailDtos = new ArrayList<>();
            for (MUser mUser:users) {
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(mUser.getId());
                UserDetailDto userDetailDto = new UserDetailDto();
                userDetailDto.setId(mUser.getId());
                userDetailDto.setFullName(mUserDetail.getFullName());
                userDetailDto.setUserName(mUser.getUsername());
                userDetailDto.setNik(mUserDetail.getNik());
                userDetailDto.setDateOfBirth(DateUtils.dateToStringUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, mUserDetail.getDateOfBirth()));
                userDetailDto.setPlaceOfBirth(mUserDetail.getPlaceOfBirth());
                userDetailDto.setGender(mUserDetail.getGender());
                userDetailDtos.add(userDetailDto);
            }
            response.setData(userDetailDtos);
            response.setIsSuccess(true);
            log.info("View Patient Succesfull",users);
        }catch (Exception e){
            log.error("View Patient Failed",e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public DeletePatientResponse deletePatient(DeletePatientRequest request) {
        DeletePatientResponse response = new DeletePatientResponse();
        try{
            MUser mUser = this.mUserRepository.findById(request.getId()).orElse(null);
            if (mUser == null){
                throw new RuntimeException("Data m_user doesn't exists");
            }
            mUser.setIsActive(false);
            mUser.setIsDeleted(true);
            mUser.setUpdatedDate(Instant.now());
            mUser.setUpdatedBy(request.getUserSession().getUserId());
            this.mUserRepository.save(mUser);
            MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(mUser.getId());
            if (mUserDetail == null){
                throw new RuntimeException("Data m_user_detail doesn't exists");
            }
            mUserDetail.setIsActive(false);
            mUserDetail.setIsDeleted(true);
            mUserDetail.setUpdatedDate(Instant.now());
            mUserDetail.setUpdatedBy(request.getUserSession().getUserId());
            this.mUserDetailService.save(mUserDetail);
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0003, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0008)));
            log.info("Delete pasien successful, result: {}", mUser);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Delete pasien failed, message: {}", e.getMessage());
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0006, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0008)));
        }
        return response;
    }
}
