package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.model.MUserDetail;
import com.bnn.rehabilitation.model.TMaterial;
import com.bnn.rehabilitation.repository.TMaterialRepository;
import com.bnn.rehabilitation.service.TMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional
public class TMaterialServiceImpl implements TMaterialService {

    private final TMaterialRepository tMaterialRepository;

    public TMaterialServiceImpl(TMaterialRepository tMaterialRepository) {
        this.tMaterialRepository = tMaterialRepository;
    }

    @Override
    public TMaterial save(TMaterial tMaterial) {
        try {
            log.info("Save TMaterial Is Success");
            return this.tMaterialRepository.save(tMaterial);
        }catch(Exception e){
            e.printStackTrace();
            log.error("Save TMaterial Failed,error:{}",e.getMessage());
            throw new RuntimeException("Save TMaterial Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<TMaterial> findAllByCounselorId(String counselorId) {
        try{
            List<TMaterial> tMaterials = this.tMaterialRepository.findAllByCounselorId(counselorId);
            log.info("Find All t_material By Counselor Id Is Success");
            return tMaterials;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find All t_material By Counselor Id Failed,error:{}",e.getMessage());
            throw new RuntimeException("Find t_material By Counselor Id Failed,error:" + e.getMessage());
        }
    }
}
