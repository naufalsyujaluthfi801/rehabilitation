package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.dto.FileDto;
import com.bnn.rehabilitation.dto.TaskDetailDto;
import com.bnn.rehabilitation.dto.TaskPatientDetailDto;
import com.bnn.rehabilitation.model.MActivityDetail;
import com.bnn.rehabilitation.model.MTaskPatientDetail;
import com.bnn.rehabilitation.repository.MTaskPatientDetailRepository;
import com.bnn.rehabilitation.service.MTaskPatientDetailService;
import com.bnn.rehabilitation.service.MUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional
public class MTaskPatientDetailServiceImpl implements MTaskPatientDetailService {

    private final MTaskPatientDetailRepository mTaskPatientDetailRepository;
    private final MUploadService mUploadService;

    public MTaskPatientDetailServiceImpl(MTaskPatientDetailRepository mTaskPatientDetailRepository, MUploadService mUploadService) {
        this.mTaskPatientDetailRepository = mTaskPatientDetailRepository;
        this.mUploadService = mUploadService;
    }

    @Override
    public MTaskPatientDetail save(MTaskPatientDetail mTaskPatientDetail) {
        try {
            log.info("Save Task Patient Detail Is Success");
            return this.mTaskPatientDetailRepository.save(mTaskPatientDetail);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Save Task Patient Detail Failed,error:{}", e.getMessage());
            throw new RuntimeException("Save Task Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public MTaskPatientDetail findOneById(String id) {
        try{
            MTaskPatientDetail mTaskPatientDetail = this.mTaskPatientDetailRepository.findById(id).orElse(null);
            log.info("Get Task Patient Detail Is Success");
            return mTaskPatientDetail;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Get Task Patient Detail Failed,error:{}",e.getMessage());
            throw new RuntimeException("Get Task Patient Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<TaskPatientDetailDto> findAllByTaskPatientId(String taskPatientId) {
        try {
            List<TaskPatientDetailDto> taskPatientDetailDtos = new ArrayList<>();
            List<MTaskPatientDetail> mTaskPatientDetails = this.mTaskPatientDetailRepository.findAllByTaskPatientId(taskPatientId);
            for (MTaskPatientDetail mTaskPatientDetail : mTaskPatientDetails) {
                FileDto fileDto = this.mUploadService.viewByUploadId(mTaskPatientDetail.getUpload().getId());
                TaskPatientDetailDto taskPatientDetailDto = new TaskPatientDetailDto();
                taskPatientDetailDto.setId(mTaskPatientDetail.getId());
                taskPatientDetailDto.setUploadId(mTaskPatientDetail.getUpload().getId());
                taskPatientDetailDto.setFileDto(fileDto);
                taskPatientDetailDto.setIsDeleted(mTaskPatientDetail.getIsDeleted());
                taskPatientDetailDtos.add(taskPatientDetailDto);
            }
            log.info("Get Task Patient Detail By Task Patient Id Is Succes");
            return taskPatientDetailDtos;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get Task Patient Detail By Task Patient Id Failed:{}", e.getMessage());
            throw new RuntimeException("Get Task Patient Detail By Task Patient Id Failed:" + e.getMessage());
        }
    }
}
