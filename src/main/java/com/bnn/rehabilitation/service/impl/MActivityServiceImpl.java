package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.constant.CommonConstant;
import com.bnn.rehabilitation.constant.DateConstant;
import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.constant.ModuleSidebarConstant;
import com.bnn.rehabilitation.dto.ActivityDetailDto;
import com.bnn.rehabilitation.dto.ActivityDto;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.model.MActivity;
import com.bnn.rehabilitation.model.MActivityDetail;
import com.bnn.rehabilitation.model.MUpload;
import com.bnn.rehabilitation.model.MUserDetail;
import com.bnn.rehabilitation.repository.MActivityRepository;
import com.bnn.rehabilitation.service.MActivityDetailService;
import com.bnn.rehabilitation.service.MActivityService;
import com.bnn.rehabilitation.service.MUploadService;
import com.bnn.rehabilitation.service.MUserDetailService;
import com.bnn.rehabilitation.utils.DateUtils;
import com.bnn.rehabilitation.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@Transactional
public class MActivityServiceImpl implements MActivityService {

    private final MActivityRepository mActivityRepository;
    private final MUploadService mUploadService;
    private final MActivityDetailService mActivityDetailService;
    private final MUserDetailService mUserDetailService;


    public MActivityServiceImpl(MActivityRepository mActivityRepository, MUploadService mUploadService, MActivityDetailService mActivityDetailService, MUserDetailService mUserDetailService) {
        this.mActivityRepository = mActivityRepository;
        this.mUploadService = mUploadService;
        this.mActivityDetailService = mActivityDetailService;
        this.mUserDetailService = mUserDetailService;
    }

    @Override
    public RegisterActivityResponse insertActivity(RegisterActivityRequest request) {
        RegisterActivityResponse response = new RegisterActivityResponse();
        try {
            MActivity mActivity = new MActivity();
            mActivity.setId(UUID.randomUUID().toString());
            mActivity.setStartTime(request.getStartTime());
            mActivity.setEndTime(request.getEndTime());
            mActivity.setDescriptionActivity(request.getDescriptionActivity());
            mActivity.setDate(new Date());
            mActivity.setIsActive(true);
            mActivity.setIsDeleted(false);
            mActivity.setCreatedDate(Instant.now());
            mActivity.setCreatedBy(request.getUserSession().getUserId());
            this.mActivityRepository.save(mActivity);
            for(var index = 0; index<request.getFiles().length; index++){
                if (!request.getFiles()[index].isEmpty()) {
                    MUpload mUpload = mUploadService.insertUpload(request.getFiles()[index], request.getUserSession());
                    MActivityDetail mActivityDetail = new MActivityDetail();
                    mActivityDetail.setId(UUID.randomUUID().toString());
                    mActivityDetail.setActivity(mActivity);
                    mActivityDetail.setUpload(mUpload);
                    mActivityDetail.setIsActive(true);
                    mActivityDetail.setIsDeleted(false);
                    mActivityDetail.setCreatedDate(Instant.now());
                    mActivityDetail.setCreatedBy(request.getUserSession().getUserId());
                    this.mActivityDetailService.save(mActivityDetail);
                }
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0003)));
            log.info("Register Activity Succesfull, result{}",mActivity);
        }catch (Exception e){
            log.info("Register Activity failed, Message{}", e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0003)));
        }
        return response;
    }

    @Override
    public ViewActivityResponse viewActivity(ViewActivityRequest request) {
        ViewActivityResponse viewActivityResponse = new ViewActivityResponse();
        try {
            Date date = null;
            if(request.getDate()==null){
                date = new Date();
            }else {
                date = DateUtils.stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004,request.getDate());
            }
            List<MActivity> mActivities = new ArrayList<>();
            if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_COUNSELOR)){
                List<MUserDetail> mUserDetails = this.mUserDetailService.findAllByCounselorId(request.getUserSession().getUserId());
                List<String> patientIds = new ArrayList<>();
                for (MUserDetail mUserDetail:mUserDetails){
                    String patientId = mUserDetail.getUser().getId();
                    patientIds.add(patientId);
                }
                mActivities = this.mActivityRepository.findAllByUserIdsPatient(patientIds, date);
            }else if(request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_INPATIENT) || request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_OUTPATIENT)){
                mActivities = this.mActivityRepository.findAllByIsActiveAndDeletedActivity(date, request.getUserSession().getUserId());
            }
            List<ActivityDto> activityDtos = new ArrayList<>();
            for (MActivity activity:mActivities){
                List<ActivityDetailDto> activityDetailDtos = this.mActivityDetailService.findAllByActivityId(activity.getId());
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(activity.getCreatedBy());
                ActivityDto activityDto = new ActivityDto();
                activityDto.setId(activity.getId());
                activityDto.setStartTime(activity.getStartTime());
                activityDto.setEndTime(activity.getEndTime());
                activityDto.setDescriptionActivity(activity.getDescriptionActivity());
                activityDto.setFullName(mUserDetail.getFullName());
                activityDto.setActivityDetailDtos(activityDetailDtos);
                activityDtos.add(activityDto);
            }
            viewActivityResponse.setData(activityDtos);
            viewActivityResponse.setIsSuccess(true);
            log.info("View Activity Succesfull",mActivities);
        }catch (Exception e){
            log.error("View Activity Failed",e.getMessage());
            e.printStackTrace();
            viewActivityResponse.setIsSuccess(false);
            viewActivityResponse.setMessage(e.getMessage());
        }
        return viewActivityResponse;
    }

    @Override
    public ModifyActivityResponse modifyActivity(ModifyActivityRequest request) {
        ModifyActivityResponse response = new ModifyActivityResponse();
        try {
            MActivity mActivity = this.mActivityRepository.findById(request.getId()).orElse(null);
            if (mActivity == null)
                throw new Exception("Data m_activity doesn't exists");
            if (request.getStarTime()!= null && !request.getStarTime().isEmpty()){
                mActivity.setStartTime(request.getStarTime());
            }
            if (request.getEndTime()!= null && !request.getEndTime().isEmpty()){
                mActivity.setEndTime(request.getEndTime());
            }
            if (request.getDescriptionActivity()!= null && !request.getDescriptionActivity().isEmpty()){
                mActivity.setDescriptionActivity(request.getDescriptionActivity());
            }
            mActivity.setCreatedDate(Instant.now());
            mActivity.setCreatedBy(request.getUserSession().getUserId());
            this.mActivityRepository.save(mActivity);
            // Buat Proces insert File Baru//
            for (var index = 0; index<request.getFiles().length; index++){
                if (!request.getFiles()[index].isEmpty()){
                    MUpload mUpload = this.mUploadService.insertUpload(request.getFiles()[index], request.getUserSession());
                    MActivityDetail mActivityDetail = new MActivityDetail();
                    mActivityDetail.setId(UUID.randomUUID().toString());
                    mActivityDetail.setActivity(mActivity);
                    mActivityDetail.setUpload(mUpload);
                    mActivityDetail.setIsActive(true);
                    mActivityDetail.setIsDeleted(false);
                    mActivityDetail.setCreatedDate(Instant.now());
                    mActivityDetail.setCreatedBy(request.getUserSession().getUserId());
                    this.mActivityDetailService.save(mActivityDetail);
                }
            }
            // Buat Delete File Data yang sudah ada//
            if (request.getActivityDetailDtos()!= null && !request.getActivityDetailDtos().isEmpty()){
                for (ActivityDetailDto activityDetailDto:request.getActivityDetailDtos()) {
                    if (activityDetailDto.getIsDeleted() == true){
                        MActivityDetail mActivityDetail = this.mActivityDetailService.findOneById(activityDetailDto.getId());
                        if (mActivityDetail != null){
                            mActivityDetail.setIsActive(false);
                            mActivityDetail.setIsDeleted(true);
                            mActivityDetail.setUpdatedBy(request.getUserSession().getUserId());
                            mActivityDetail.setUpdatedDate(Instant.now());
                            this.mActivityDetailService.save(mActivityDetail);
                        }
                    }
                }
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0002, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0002)));
            log.info("Modify activity successful, result: {}",mActivity);
        }catch (Exception e){
            e.printStackTrace();
            log.info("Modify activity failed, message :{}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0005, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0002)));
        }
        return response;
    }

    @Override
    public LoadModifyActivityResponse loadModify(LoadModifyActivityRequest request) {
        LoadModifyActivityResponse response = new LoadModifyActivityResponse();
        try {
            MActivity mActivity = this.mActivityRepository.findById(request.getId()).orElse(null);
            if(mActivity == null)
                throw new Exception("Data activity doesn't exist with id: " + request.getId());
            ActivityDto activityDto = new ActivityDto();
            List<ActivityDetailDto> activityDetailDtos = this.mActivityDetailService.findAllByActivityId(mActivity.getId());
            activityDto.setId(mActivity.getId());
            activityDto.setStartTime(mActivity.getStartTime());
            activityDto.setEndTime(mActivity.getEndTime());
            activityDto.setDescriptionActivity(mActivity.getDescriptionActivity());
            activityDto.setActivityDetailDtos(activityDetailDtos);
            response.setData(activityDto);
            response.setIsSuccess(true);
        }catch (Exception e){
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public DeleteActivityResponse deleteActivity(DeleteActivityRequest request) {
        DeleteActivityResponse response = new DeleteActivityResponse();
        try{
            MActivity mActivity = this.mActivityRepository.findById(request.getId()).orElse(null);
            if(mActivity == null)
                throw new Exception("Data m_activity doesn't exists");
            mActivity.setIsActive(false);
            mActivity.setIsDeleted(true);
            mActivity.setUpdatedDate(Instant.now());
            mActivity.setUpdatedBy(request.getUserSession().getUserId());
            this.mActivityRepository.save(mActivity);
            List<MActivityDetail> mActivityDetails = this.mActivityDetailService.findByActivityId(mActivity.getId());
            for (MActivityDetail mActivityDetail:mActivityDetails) {
                mActivityDetail.setIsActive(false);
                mActivityDetail.setIsDeleted(true);
                mActivityDetail.setUpdatedDate(Instant.now());
                mActivityDetail.setUpdatedBy(request.getUserSession().getUserId());
                this.mActivityDetailService.save(mActivityDetail);
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0003, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0002)));
            log.info("Delete activity successful, result: {}", mActivity);
        }catch (Exception e){
            e.printStackTrace();
            log.info("Delete activity failed, message: {}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0006, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0002)));
        }
        return response;
    }

}
