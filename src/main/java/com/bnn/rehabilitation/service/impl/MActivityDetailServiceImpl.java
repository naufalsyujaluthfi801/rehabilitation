package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.dto.ActivityDetailDto;
import com.bnn.rehabilitation.dto.FileDto;
import com.bnn.rehabilitation.model.MActivityDetail;
import com.bnn.rehabilitation.repository.MActivityDetailRepository;
import com.bnn.rehabilitation.service.MActivityDetailService;
import com.bnn.rehabilitation.service.MUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional
public class MActivityDetailServiceImpl implements MActivityDetailService {

    private final MActivityDetailRepository mActivityDetailRepository;
    private final MUploadService mUploadService;

    public MActivityDetailServiceImpl(MActivityDetailRepository mActivityDetailRepository, MUploadService mUploadService) {
        this.mActivityDetailRepository = mActivityDetailRepository;
        this.mUploadService = mUploadService;
    }

    @Override
    public MActivityDetail save(MActivityDetail mActivityDetail) {
        try {
            log.info("Save Activity Detail Is Success");
            return this.mActivityDetailRepository.save(mActivityDetail);
        }catch(Exception e){
            e.printStackTrace();
            log.error("Save Activity Detail Failed,error:{}",e.getMessage());
            throw new RuntimeException("Save Activity Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public MActivityDetail findOneById(String id) {
        try{
            MActivityDetail mActivityDetail = this.mActivityDetailRepository.findById(id).orElse(null);
            log.info("Get Activity Detail Is Success");
            return mActivityDetail;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Get Activity Detail Failed,error:{}",e.getMessage());
            throw new RuntimeException("Get Activity Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<ActivityDetailDto> findAllByActivityId(String activityId) {
        try {
            List<ActivityDetailDto> activityDetailDtos = new ArrayList<>();
            List<MActivityDetail> mActivityDetails = this.mActivityDetailRepository.findByActivityId(activityId);
            for (MActivityDetail mActivityDetail:mActivityDetails){
                FileDto fileDto = this.mUploadService.viewByUploadId(mActivityDetail.getUpload().getId());
                ActivityDetailDto activityDetailDto = new ActivityDetailDto();
                activityDetailDto.setId(mActivityDetail.getId());
                activityDetailDto.setActivityId(mActivityDetail.getActivity().getId());
                activityDetailDto.setUploadId(mActivityDetail.getUpload().getId());
                activityDetailDto.setFileDto(fileDto);
                activityDetailDto.setIsDeleted(mActivityDetail.getIsDeleted());
                activityDetailDtos.add(activityDetailDto);
            }
            log.info("Get Activity By ActivityId Is Success");
            return activityDetailDtos;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Get Activity By ActivityId Failed:{}",e.getMessage());
            throw new RuntimeException("Get Activity By ActivityId Failed:" + e.getMessage());
        }
    }

    @Override
    public List<MActivityDetail> findByActivityId(String activityId) {
        try {
            log.info("Find Activity Detail By ActivityId Is Success ");
            return this.mActivityDetailRepository.findByActivityId(activityId);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find Activity Detail By ActivityId Failed:{}",e.getMessage());
            throw new RuntimeException("Find Activity Detail By ActivityId failed:" + e.getMessage());
        }

    }
}
