package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.dto.FileDto;
import com.bnn.rehabilitation.dto.MaterialDetailDto;
import com.bnn.rehabilitation.model.MMaterialDetail;
import com.bnn.rehabilitation.repository.MMaterialDetailRepository;
import com.bnn.rehabilitation.service.MMaterialDetailService;
import com.bnn.rehabilitation.service.MUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional

public class MMaterialDetailServiceImpl implements MMaterialDetailService {

    private final MMaterialDetailRepository mMaterialDetailRepository;
    private final MUploadService mUploadService;

    public MMaterialDetailServiceImpl(MMaterialDetailRepository mMaterialDetailRepository, MUploadService mUploadService) {
        this.mMaterialDetailRepository = mMaterialDetailRepository;
        this.mUploadService = mUploadService;
    }

    @Override
    public MMaterialDetail save(MMaterialDetail mMaterialDetail) {
        try {
            log.info("Save Material Detail Is Success");
            return this.mMaterialDetailRepository.save(mMaterialDetail);
        }catch(Exception e){
            e.printStackTrace();
            log.error("Save Material Detail Failed,error:{}",e.getMessage());
            throw new RuntimeException("Save Material Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public MMaterialDetail findOneById(String id) {
        try{
            MMaterialDetail mMaterialDetail = this.mMaterialDetailRepository.findById(id).orElse(null);
            log.info("Get Material Detail Is Success");
            return mMaterialDetail;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Get Material Detail Failed,error:{}",e.getMessage());
            throw new RuntimeException("Get Material Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<MaterialDetailDto> findAllByMaterialId(String materialId) {
        try {
            List<MaterialDetailDto> materialDetailDtos = new ArrayList<>();
            List<MMaterialDetail> mMaterialDetails = this.mMaterialDetailRepository.findByMaterialId(materialId);
            for (MMaterialDetail mMaterialDetail : mMaterialDetails) {
                FileDto fileDto = this.mUploadService.viewByUploadId(mMaterialDetail.getUpload().getId());
                MaterialDetailDto materialDetailDto = new MaterialDetailDto();
                materialDetailDto.setId(mMaterialDetail.getId());
                materialDetailDto.setMaterialId(mMaterialDetail.getMaterial().getId());
                materialDetailDto.setUploadId(mMaterialDetail.getUpload().getId());
                materialDetailDto.setFileDto(fileDto);
                materialDetailDtos.add(materialDetailDto);
            }
            log.info("Get Material By MaterialId Is Succes");
            return materialDetailDtos;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get Material By MaterialId Failed:{}", e.getMessage());
            throw new RuntimeException("Get Material By MaterialId Failed:" + e.getMessage());
        }
    }

    @Override
    public List<MMaterialDetail> findByMaterialId(String materialId) {
        try {
            log.info("Find Material Detail By MaterialId Is Succes ");
            return this.mMaterialDetailRepository.findByMaterialId(materialId);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Find Material By Detail By ActivityId Failed:{}" + e.getMessage());
            throw new RuntimeException("Find Material By Detail By ActivityId Failed:" + e.getMessage());
        }
    }
}
