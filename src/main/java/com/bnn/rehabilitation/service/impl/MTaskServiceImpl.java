package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.constant.CommonConstant;
import com.bnn.rehabilitation.constant.DateConstant;
import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.constant.ModuleSidebarConstant;
import com.bnn.rehabilitation.dto.*;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.model.*;
import com.bnn.rehabilitation.repository.*;
import com.bnn.rehabilitation.service.*;
import com.bnn.rehabilitation.utils.DateUtils;
import com.bnn.rehabilitation.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@Transactional


public class MTaskServiceImpl implements MTaskService {
    private final MTaskRepository mTaskRepository;
    private final MUploadService mUploadService;
    private final MTaskDetailService mTaskDetailService;
    private final MRoleRepository mRoleRepository;
    private final MUserRepository mUserRepository;
    private final MUserDetailService mUserDetailService;
    private final TTaskService tTaskService;
    private final MTaskPatientDetailService mTaskPatientDetailService;
    private final MTaskPatientService mTaskPatientService;

    public MTaskServiceImpl(MTaskRepository mTaskRepository, MUploadService mUploadService, MTaskDetailService mTaskDetailService,
                            MRoleRepository mRoleRepository, MUserRepository mUserRepository, MUserDetailService mUserDetailService,
                            TTaskService tTaskService, MTaskPatientDetailService mTaskPatientDetailService, MTaskPatientService mTaskPatientService) {
        this.mTaskRepository = mTaskRepository;
        this.mUploadService = mUploadService;
        this.mTaskDetailService = mTaskDetailService;
        this.mRoleRepository = mRoleRepository;
        this.mUserRepository = mUserRepository;
        this.mUserDetailService = mUserDetailService;
        this.tTaskService = tTaskService;
        this.mTaskPatientDetailService = mTaskPatientDetailService;
        this.mTaskPatientService = mTaskPatientService;
    }


    @Override
    public RegisterTaskResponse insertTask(RegisterTaskRequest request) {
        RegisterTaskResponse response = new RegisterTaskResponse();
        try {
            MTask mTask = new MTask();
            mTask.setId(UUID.randomUUID().toString());
            mTask.setDescriptionTask(request.getDescriptionTask());
            mTask.setDate(new Date());
            mTask.setIsActive(true);
            mTask.setIsDeleted(false);
            mTask.setCreatedDate(Instant.now());
            mTask.setCreatedBy(request.getUserSession().getUserId());
            this.mTaskRepository.save(mTask);
            for(var index = 0; index<request.getFiles().length; index++){
                if (!request.getFiles()[index].isEmpty()) {
                    MUpload mUpload = mUploadService.insertUpload(request.getFiles()[index], request.getUserSession());
                    MTaskDetail mTaskDetail = new MTaskDetail();
                    mTaskDetail.setId(UUID.randomUUID().toString());
                    mTaskDetail.setTask(mTask);
                    mTaskDetail.setUpload(mUpload);
                    mTaskDetail.setIsActive(true);
                    mTaskDetail.setIsDeleted(false);
                    mTaskDetail.setCreatedDate(Instant.now());
                    mTaskDetail.setCreatedBy(request.getUserSession().getUserId());
                    this.mTaskDetailService.save(mTaskDetail);
                }
            }
            List<String> counselorIds = request.getCounselorIds();
            for (String counselorId:counselorIds) {
                TTask tTask = new TTask();
                tTask.setId(UUID.randomUUID().toString());
                tTask.setTask(mTask);
                tTask.setCounselorId(counselorId);
                tTask.setIsActive(true);
                tTask.setIsDeleted(false);
                tTask.setCreatedDate(Instant.now());
                tTask.setCreatedBy(request.getUserSession().getUserId());
                this.tTaskService.save(tTask);
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0006)));
            log.info("Register Task Succesfull, result{}",mTask);

        }catch (Exception e){
            log.info("Register Task failed, Message{}", e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0006)));
        }
        return response;
    }

    @Override
    public LoadRegisterTaskResponse loadTask(LoadRegisterTaskRequest request) {
        LoadRegisterTaskResponse response = new LoadRegisterTaskResponse();
        try {
            List<UserDto> userDtoList = new ArrayList<>();
            MRole counselorRole = this.mRoleRepository.findOneByCode(CommonConstant.ROLE_CODE_COUNSELOR);
            List<MUser> users = this.mUserRepository.findAllByRoleId(counselorRole.getId());
            for (MUser mUser:users) {
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(mUser.getId());
                String fullname = mUser.getUsername();
                if (mUserDetail!=null && !mUserDetail.getFullName().isEmpty()){
                    fullname = mUserDetail.getFullName();
                }
                UserDto userDto = new UserDto();
                userDto.setId(mUser.getId());
                userDto.setUserName(mUser.getUsername());
                userDto.setFullName(fullname);
                userDtoList.add(userDto);
            }
            response.setIsSuccess(true);
            response.setUserDtoList(userDtoList);
            log.info("Load register Task successful");
        }catch (Exception e){
            log.error("Load register Task failed, error: {}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public ViewTaskResponse viewTask(ViewTaskRequest request) {
        ViewTaskResponse viewTaskResponse = new ViewTaskResponse();
        try {
            Date date = null;
            if(request.getDate()==null){
                date = new Date();
            }else {
                date = DateUtils.stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004,request.getDate());
            }
            List<MTask> mTasks = new ArrayList<>();
            if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_COUNSELOR)){
                List<TTask> tTasks = this.tTaskService.findAllByCounselorId(request.getUserSession().getUserId());
                List<String> taskIds = new ArrayList<>();
                for (TTask tTask:tTasks) {
                    String taskId = tTask.getTask().getId();
                    taskIds.add(taskId);
                }
                mTasks = this.mTaskRepository.findAllByTasklIds(taskIds, date);
            }else if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_INPATIENT) || request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_OUTPATIENT)){
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(request.getUserSession().getUserId());
                List<TTask> tTasks = this.tTaskService.findAllByCounselorId(mUserDetail.getCounselorId());
                List<String> taskIds = new ArrayList<>();
                for (TTask tTask:tTasks) {
                    String taskId = tTask.getTask().getId();
                    taskIds.add(taskId);
                }
                mTasks = this.mTaskRepository.findAllByTasklIds(taskIds, date);
            }else if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_ASSISTANT_COUNSELOR)){
                mTasks = this.mTaskRepository.findAllByIsActiveAndDeletedTask(date);
            }
            List<TaskDto> taskDtos = new ArrayList<>();
            for (MTask task:mTasks) {
                TaskDto taskDto = new TaskDto();
                List<TaskDetailDto> taskDetailDtos = this.mTaskDetailService.findAllByTaskId(task.getId());
                if (request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_INPATIENT) ||
                        request.getUserSession().getCode().equals(CommonConstant.ROLE_CODE_OUTPATIENT)){
                    List<MTaskPatient> mTaskPatients = this.mTaskPatientService.findAllByTaskIdAndUserId(task.getId(), request.getUserSession().getUserId());
                    List<TaskPatientDto> taskPatientDtos = new ArrayList<>();
                    for (MTaskPatient mTaskPatient:mTaskPatients) {
                        List<TaskPatientDetailDto> taskPatientDetailDtos = this.mTaskPatientDetailService.findAllByTaskPatientId(mTaskPatient.getId());
                        TaskPatientDto taskPatientDto = new TaskPatientDto();
                        taskPatientDto.setDescriptionTask(mTaskPatient.getDescriptionTask());
                        taskPatientDto.setTaskPatientDetailDtos(taskPatientDetailDtos);
                        taskPatientDtos.add(taskPatientDto);
                    }
                    taskDto.setTaskPatientDtos(taskPatientDtos);
                }
                taskDto.setId(task.getId());
                taskDto.setDescriptionTask(task.getDescriptionTask());
                taskDto.setTaskDetailDtos(taskDetailDtos);
                taskDtos.add(taskDto);
            }
            viewTaskResponse.setData(taskDtos);
            viewTaskResponse.setIsSuccess(true);
            log.info("View Task Succesfull",mTasks);
        }catch (Exception e){
            log.error("View Task Failed",e.getMessage());
            e.printStackTrace();
            viewTaskResponse.setIsSuccess(false);
            viewTaskResponse.setMessage(e.getMessage());
        }
        return viewTaskResponse;
    }

    @Override
    public ModifyTaskPatientResponse modifyTask(ModifyTaskPatientRequest request) {
        ModifyTaskPatientResponse response = new ModifyTaskPatientResponse();
        try {
            MTask mTask = this.mTaskRepository.findById(request.getId()).orElse(null);
            if (mTask == null)
                throw new Exception("Data m_task doesn't exists");
            if (request.getDescriptionTask()!= null && !request.getDescriptionTask().isEmpty()){
                mTask.setDescriptionTask(request.getDescriptionTask());
            }
            mTask.setCreatedDate(Instant.now());
            mTask.setCreatedBy(request.getUserSession().getUserId());
            this.mTaskRepository.save(mTask);
            // Buat Proces insert File Baru//
            for (var index = 0; index<request.getFiles().length; index++){
                if (!request.getFiles()[index].isEmpty()){
                    MUpload mUpload = this.mUploadService.insertUpload(request.getFiles()[index], request.getUserSession());
                    MTaskDetail mTaskDetail = new MTaskDetail();
                    mTaskDetail.setId(UUID.randomUUID().toString());
                    mTaskDetail.setTask(mTask);
                    mTaskDetail.setUpload(mUpload);
                    mTaskDetail.setIsActive(true);
                    mTaskDetail.setIsDeleted(false);
                    mTaskDetail.setCreatedDate(Instant.now());
                    mTaskDetail.setCreatedBy(request.getUserSession().getUserId());
                    this.mTaskDetailService.save(mTaskDetail);
                }
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0002, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0007)));
            log.info("Modify Task successful, result: {}",mTask);
        }catch (Exception e){
            e.printStackTrace();
            log.info("Modify Task failed, message :{}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0005, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0007)));
        }
        return response;
    }

    @Override
    public LoadModifyTaskPatientResponse loadModify(LoadModifyTaskPatientRequest request) {
        LoadModifyTaskPatientResponse response = new LoadModifyTaskPatientResponse();
        try {
            MTask mTask = this.mTaskRepository.findById(request.getId()).orElse(null);
            if(mTask == null)
                throw new Exception("Data task doesn't exist with id: " + request.getId());
            TaskDto taskDto = new TaskDto();
            List<TaskDetailDto> taskDetailDtos = this.mTaskDetailService.findAllByTaskId(mTask.getId());
            taskDto.setId(mTask.getId());
            taskDto.setDescriptionTask(mTask.getDescriptionTask());
            taskDto.setTaskDetailDtos(taskDetailDtos);
            response.setData(taskDto);
            response.setIsSuccess(true);
        }catch (Exception e){
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public DeleteTaskResponse deleteTask(DeleteTaskRequest request) {
        DeleteTaskResponse response = new DeleteTaskResponse();
        try{
            MTask mTask = this.mTaskRepository.findById(request.getId()).orElse(null);
            if(mTask == null)
                throw new Exception("Data m_task doesn't exists");
            mTask.setIsActive(false);
            mTask.setIsDeleted(true);
            mTask.setUpdatedDate(Instant.now());
            mTask.setUpdatedBy(request.getUserSession().getUserId());
            this.mTaskRepository.save(mTask);
            List<MTaskDetail> mTaskDetails = this.mTaskDetailService.findByTaskId(mTask.getId());
            for (MTaskDetail mTaskDetail:mTaskDetails) {
                mTaskDetail.setIsActive(false);
                mTaskDetail.setIsDeleted(true);
                mTaskDetail.setUpdatedDate(Instant.now());
                mTaskDetail.setUpdatedBy(request.getUserSession().getUserId());
                this.mTaskDetailService.save(mTaskDetail);
            }
            List<TTask> tTasks = this.tTaskService.findAllByTaskId(mTask.getId());
            for (TTask tTask:tTasks) {
                tTask.setIsActive(false);
                tTask.setIsDeleted(true);
                tTask.setUpdatedDate(Instant.now());
                tTask.setUpdatedBy(request.getUserSession().getUserId());
                this.tTaskService.save(tTask);
            }
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0003, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0006)));
            log.info("Delete task successful, result: {}", mTask);
        }catch (Exception e){
            e.printStackTrace();
            log.info("Delete task failed, message: {}", e.getMessage());
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0006, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0006)));
        }
        return response;
    }

    @Override
    public ViewTaskPatientResponse viewTaskPatient(ViewTaskPatientRequest request) {
        ViewTaskPatientResponse response = new ViewTaskPatientResponse();
        try {
            MTask mTask = this.mTaskRepository.findById(request.getId()).orElse(null);
            if (mTask == null){
                throw new RuntimeException("Data mTask doesn't exists ");
            }
            TaskDto taskDto = new TaskDto();
            List<TaskDetailDto> taskDetailDtos = this.mTaskDetailService.findAllByTaskId(mTask.getId());
            List<MTaskPatient> mTaskPatients = this.mTaskPatientService.findAllByTaskId(mTask.getId());
            List<TaskPatientDto> taskPatientDtos = new ArrayList<>();
            for (MTaskPatient mTaskPatient:mTaskPatients) {
                MUserDetail mUserDetail = this.mUserDetailService.findOneByUserId(mTaskPatient.getCreatedBy());
                List<TaskPatientDetailDto> taskPatientDetailDtos = this.mTaskPatientDetailService.findAllByTaskPatientId(mTaskPatient.getId());
                TaskPatientDto taskPatientDto = new TaskPatientDto();
                taskPatientDto.setDescriptionTask(mTaskPatient.getDescriptionTask());
                taskPatientDto.setFullName(mUserDetail.getFullName());
                taskPatientDto.setTaskPatientDetailDtos(taskPatientDetailDtos);
                taskPatientDtos.add(taskPatientDto);
            }
            taskDto.setTaskPatientDtos(taskPatientDtos);
            taskDto.setId(mTask.getId());
            taskDto.setDescriptionTask(mTask.getDescriptionTask());
            taskDto.setTaskDetailDtos(taskDetailDtos);
            response.setData(taskDto);
            response.setIsSuccess(true);
            log.info("View Task Succesfull",mTask);
        }catch (Exception e){
            log.error("View Task Failed",e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

}

