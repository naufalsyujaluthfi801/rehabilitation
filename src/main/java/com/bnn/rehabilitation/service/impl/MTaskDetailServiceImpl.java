package com.bnn.rehabilitation.service.impl;

import com.bnn.rehabilitation.dto.FileDto;
import com.bnn.rehabilitation.dto.MaterialDetailDto;
import com.bnn.rehabilitation.dto.TaskDetailDto;
import com.bnn.rehabilitation.model.MMaterialDetail;
import com.bnn.rehabilitation.model.MTaskDetail;
import com.bnn.rehabilitation.repository.MMaterialDetailRepository;
import com.bnn.rehabilitation.repository.MTaskDetailRepository;
import com.bnn.rehabilitation.service.MTaskDetailService;
import com.bnn.rehabilitation.service.MUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional

public class MTaskDetailServiceImpl implements MTaskDetailService {
    private final MTaskDetailRepository mTaskDetailRepository;
    private final MUploadService mUploadService;

    public MTaskDetailServiceImpl(MTaskDetailRepository mTaskDetailRepository, MUploadService mUploadService) {
        this.mTaskDetailRepository = mTaskDetailRepository;
        this.mUploadService = mUploadService;
    }

    @Override
    public MTaskDetail save(MTaskDetail mTaskDetail) {
        try {
            log.info("Save Task Detail Is Success");
            return this.mTaskDetailRepository.save(mTaskDetail);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Save Task Detail Failed,error:{}", e.getMessage());
            throw new RuntimeException("Save Task Detail Failed,error:" + e.getMessage());
        }

    }

    @Override
    public MTaskDetail findOneById(String id) {
        try {
            MTaskDetail mTaskDetail = this.mTaskDetailRepository.findById(id).orElse(null);
            log.info("Get Task Detail Is Success");
            return mTaskDetail;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get Task Detail Failed,error:{}", e.getMessage());
            throw new RuntimeException("Get Task Detail Failed,error:" + e.getMessage());
        }
    }

    @Override
    public List<TaskDetailDto> findAllByTaskId(String taskId) {
        try {
            List<TaskDetailDto> taskDetailDtos = new ArrayList<>();
            List<MTaskDetail> mTaskDetails = this.mTaskDetailRepository.findByTaskId(taskId);
            for (MTaskDetail mTaskDetail : mTaskDetails) {
                FileDto fileDto = this.mUploadService.viewByUploadId(mTaskDetail.getUpload().getId());
                TaskDetailDto taskDetailDto = new TaskDetailDto();
                taskDetailDto.setId(mTaskDetail.getId());
                taskDetailDto.setTaskId(mTaskDetail.getTask().getId());
                taskDetailDto.setUploadId(mTaskDetail.getUpload().getId());
                taskDetailDto.setFileDto(fileDto);
                taskDetailDto.setIsDeleted(mTaskDetail.getIsDeleted());
                taskDetailDtos.add(taskDetailDto);
            }
            log.info("Get Task By TaskId Is Succes");
            return taskDetailDtos;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get Task By TaskId Failed:{}", e.getMessage());
            throw new RuntimeException("Get Task By TaskId Failed:" + e.getMessage());
        }
    }

    @Override
    public List<MTaskDetail> findByTaskId(String taskId) {
        try {
            log.info("Find Task Detail By TaskId Is Succes ");
            return this.mTaskDetailRepository.findByTaskId(taskId);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Find Task By Detail By TaskId Failed:{}" + e.getMessage());
            throw new RuntimeException("Find Task By Detail By TaskId Failed:" + e.getMessage());
        }
    }

}

