package com.bnn.rehabilitation.helper;

import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.model.MUser;
import com.bnn.rehabilitation.model.MUserDetail;
import com.bnn.rehabilitation.repository.MUserDetailRepository;
import com.bnn.rehabilitation.repository.MUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class AuthenticationHelper {

    private final MUserRepository userRepository;
    private final MUserDetailRepository userDetailRepository;

    public AuthenticationHelper(MUserRepository userRepository, MUserDetailRepository userDetailRepository) {
        this.userRepository = userRepository;
        this.userDetailRepository = userDetailRepository;
    }

    public UserSessionDto getUserSession() {
        UserSessionDto userSession = new UserSessionDto();
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
            List<String> authorityList = new ArrayList<String>();
            for (GrantedAuthority a : authorities) {
                authorityList.add(a.getAuthority());
            }
            MUser user = this.userRepository.findMUserByUsername(username);
            MUserDetail userDetail = this.userDetailRepository.findOneByUserId(user.getId());
            userSession = new UserSessionDto(user, authorityList, userDetail.getFullName());
            log.info("Get user session successful, result: {}", userSession);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get user session failed, message: {}", e);
        }
        return userSession;
    }

    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || AnonymousAuthenticationToken.class.
                isAssignableFrom(authentication.getClass())) {
            return false;
        }
        return authentication.isAuthenticated();
    }

}
