package com.bnn.rehabilitation.validator;

import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.form.PatientInputForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegisterPatientValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PatientInputForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PatientInputForm form = (PatientInputForm) target;
        if(form.getUserName() == null || form.getUserName().isEmpty()){
            errors.rejectValue("userName", MessageConstant.VALIDATION_MESSAGE_0011);
        }

        if(form.getPassword() == null || form.getPassword().isEmpty()){
            errors.rejectValue("password", MessageConstant.VALIDATION_MESSAGE_0003);
        }

        if(form.getPassword() != null && form.getPassword().length()<8){
            errors.rejectValue("password", MessageConstant.VALIDATION_MESSAGE_0016);
        }

        if(form.getNik() != null && form.getNik().length()!= 16){
            errors.rejectValue("nik", MessageConstant.VALIDATION_MESSAGE_0015);
        }

        if (form.getFullName() == null || form.getFullName().isEmpty()){
            errors.rejectValue("fullName", MessageConstant.VALIDATION_MESSAGE_0017);
        }

        if (form.getAddress() == null || form.getAddress().isEmpty()){
            errors.rejectValue("address", MessageConstant.VALIDATION_MESSAGE_0019);
        }

        if (form.getGender() == null || form.getGender().isEmpty()){
            errors.rejectValue("gender", MessageConstant.VALIDATION_MESSAGE_0004);
        }

        if (form.getRoleId() == null || form.getRoleId().isEmpty()){
            errors.rejectValue("roleId", MessageConstant.VALIDATION_MESSAGE_0018);
        }

        if (form.getCounselorId() == null || form.getCounselorId().isEmpty()){
            errors.rejectValue("counselorId", MessageConstant.VALIDATION_MESSAGE_0020);
        }
    }
}
