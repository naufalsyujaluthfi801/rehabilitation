package com.bnn.rehabilitation.validator;

import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.form.ActivityInputForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegisterActivityValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return ActivityInputForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ActivityInputForm form = (ActivityInputForm) target;
        if(form.getStartTime() == null || form.getStartTime().isEmpty()){
            errors.rejectValue("startTime", MessageConstant.VALIDATION_MESSAGE_0012);
        }

        if(form.getEndTime() == null || form.getEndTime().isEmpty()){
            errors.rejectValue("endTime", MessageConstant.VALIDATION_MESSAGE_0013);
        }

        if(form.getDescriptionActivity() == null || form.getDescriptionActivity().isEmpty()){
            errors.rejectValue("descriptionActivity", MessageConstant.VALIDATION_MESSAGE_0014);
        }

    }
}
