package com.bnn.rehabilitation.validator;

import com.bnn.rehabilitation.constant.MessageConstant;
import com.bnn.rehabilitation.form.SignupForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegisterCounselorValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return SignupForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SignupForm form = (SignupForm) target;
        if(form.getFullName() == null || form.getFullName().isEmpty()){
            errors.rejectValue("fullName", MessageConstant.VALIDATION_MESSAGE_0017);
        }

        if(form.getUsername() == null || form.getUsername().isEmpty()){
            errors.rejectValue("username", MessageConstant.VALIDATION_MESSAGE_0002);
        }

        if(form.getPassword() == null || form.getPassword().isEmpty()){
            errors.rejectValue("password", MessageConstant.VALIDATION_MESSAGE_0003);
        }

        if(form.getPassword() != null && form.getPassword().length()<8){
            errors.rejectValue("password", MessageConstant.VALIDATION_MESSAGE_0016);
        }

        if(form.getRoleId() == null || form.getRoleId().isEmpty()){
            errors.rejectValue("roleId", MessageConstant.VALIDATION_MESSAGE_0018);
        }
    }
}
