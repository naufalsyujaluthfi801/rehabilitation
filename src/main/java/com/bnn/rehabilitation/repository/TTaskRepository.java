package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.TMaterial;
import com.bnn.rehabilitation.model.TTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TTaskRepository extends JpaRepository <TTask, String> {
    @Query(value = "SELECT m FROM TTask m WHERE m.counselorId =:counselorId AND m.isActive = true AND m.isDeleted = false")
    List<TTask> findAllByCounselorId(String counselorId);

    @Query(value = "SELECT m FROM TTask m WHERE m.task.id =:taskId AND m.isActive = true  and m.isDeleted= false ORDER BY m.createdDate desc ")
    List<TTask> findAllByTaskId(String taskId);
}
