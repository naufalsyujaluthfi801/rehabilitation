package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MMaterialDetail;
import com.bnn.rehabilitation.model.MTaskDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MTaskDetailRepository extends JpaRepository <MTaskDetail, String> {
    @Query(value = "SELECT m FROM MTaskDetail m WHERE m.task.id = :taskId " +
            "AND m.isActive = true " +
            "AND m.isDeleted = false " +
            "order by m.createdDate desc")
    List<MTaskDetail> findByTaskId(@Param("taskId") String taskId);
}
