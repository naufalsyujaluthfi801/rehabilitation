package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MActivityDetail;
import com.bnn.rehabilitation.model.MMaterialDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MMaterialDetailRepository extends JpaRepository <MMaterialDetail, String>{
    @Query(value = "SELECT m FROM MMaterialDetail m WHERE m.material.id = :materialId")
    List<MMaterialDetail> findByMaterialId(@Param("materialId") String materialId);
}
