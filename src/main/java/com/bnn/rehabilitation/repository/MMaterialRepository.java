package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MActivity;
import com.bnn.rehabilitation.model.MMaterial;
import com.bnn.rehabilitation.model.TMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MMaterialRepository extends JpaRepository <MMaterial, String> {
    @Query(value = "SELECT m FROM MMaterial m WHERE m.date =:date")
    List<MMaterial> findByMaterialDate (Date date);

    @Query(value = "SELECT m FROM MMaterial m WHERE m.date = :date AND m.isActive = true AND m.isDeleted = false ORDER BY m.createdDate desc ")
    List<MMaterial> findAllByIsActiveAndDeletedMaterial(@Param("date") Date date);

    @Query(value = "SELECT m FROM MMaterial m WHERE m.date = :date AND m.id IN :materialIds AND m.isActive = true AND m.isDeleted = false ORDER BY m.createdDate desc ")
    List<MMaterial> findAllByMaterialIds(@Param("materialIds") List<String> materialIds, @Param("date") Date date);


}
