package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MUserRepository extends JpaRepository<MUser, String> {
    @Query("select t from MUser t where t.username =:username")
    MUser findMUserByUsername(String username);

    boolean existsMUserByUsername(String username);

    @Query("select concat('ROLE_', p.code) from MUser u inner join MRolePermission rp on rp.role = u.role inner join MPermission p on p = rp.permission where u.username =:username and p.isActive = true and p.isDeleted = false")
    List<String> findAllPermissionCodeByUsername(String username);

    @Query(value = "SELECT m FROM MUser m WHERE m.role.id =:roleId AND m.isActive = true AND m.isDeleted = FALSE ORDER BY m.createdDate desc ")
    List<MUser> findAllByRoleId(String roleId);

    @Query(value = "SELECT m FROM MUser m WHERE m.role.id IN :roleIds AND m.isActive = true AND m.isDeleted = FALSE ORDER BY m.createdDate desc ")
    List<MUser> findAllByRoleIds(List<String>roleIds);
}
