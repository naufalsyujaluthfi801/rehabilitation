package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MUpload;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MUploadRepository extends JpaRepository <MUpload, String> {
}
