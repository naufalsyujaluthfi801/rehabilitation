package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MRoleRepository extends JpaRepository<MRole, String> {
//    @Query(value = "SELECT * FROM m_role WHERE TYPE IN :types", nativeQuery = true)
//    List<MRole> findAllByTypes (List<String> types);

    @Query(value = "SELECT m FROM MRole m WHERE m.type IN :types")
    List<MRole> findAllByTypes(List<String> types);

    @Query(value = "SELECT m FROM MRole m WHERE m.code =:codes")
    MRole findOneByCode(String codes);
}
