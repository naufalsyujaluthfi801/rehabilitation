package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MTaskPatientDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MTaskPatientDetailRepository extends JpaRepository <MTaskPatientDetail, String> {
    @Query(value = "SELECT m FROM MTaskPatientDetail m WHERE m.taskPatient.id = :taskPatientId " +
            "AND m.isActive = true " +
            "AND m.isDeleted = false " +
            "order by m.createdDate desc")
    List<MTaskPatientDetail> findAllByTaskPatientId(@Param("taskPatientId") String taskPatientId);
}
