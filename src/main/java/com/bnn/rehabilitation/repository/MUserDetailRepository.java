package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MUserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MUserDetailRepository extends JpaRepository<MUserDetail, String> {
    @Query(value = "SELECT m FROM MUserDetail m WHERE m.user.id =:userId")
    MUserDetail findOneByUserId(String userId);

    @Query(value = "SELECT m FROM MUserDetail m WHERE m.counselorId =:counselorId AND m.isActive = true AND m.isDeleted = false")
    List<MUserDetail> findAllByCounselorId(String counselorId);
}
