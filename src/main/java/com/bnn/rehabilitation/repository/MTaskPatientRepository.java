package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MTaskPatient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MTaskPatientRepository extends JpaRepository <MTaskPatient, String> {
    @Query(value = "SELECT m FROM MTaskPatient m WHERE m.task.id =:taskId AND m.isActive = true AND m.isDeleted = false AND m.createdBy =:createdBy ORDER BY m.createdDate desc ")
    MTaskPatient findOneByTaskIdAndUserId(@Param("taskId") String taskId, @Param("createdBy") String createdBy);

    @Query(value = "SELECT m FROM MTaskPatient m WHERE m.task.id =:taskId AND m.createdBy =:createdBy AND m.isActive = true AND m.isDeleted = false ORDER BY m.createdDate desc ")
    List<MTaskPatient> findAllByTaskIdAndUserId(@Param("taskId") String taskId, @Param("createdBy") String createdBy);

    @Query(value = "SELECT m FROM MTaskPatient m WHERE m.task.id =:taskId AND m.isActive = true AND m.isDeleted = false ORDER BY m.createdDate desc ")
    List<MTaskPatient> findAllByTaskId(@Param("taskId") String taskId);
}
