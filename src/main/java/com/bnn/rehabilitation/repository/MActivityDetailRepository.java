package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MActivityDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MActivityDetailRepository extends JpaRepository <MActivityDetail, String> {
    @Query(value = "SELECT m FROM MActivityDetail m WHERE m.activity.id = :activityId " +
            "AND m.isActive = true " +
            "AND m.isDeleted = false " +
            "order by m.createdDate desc")
    List<MActivityDetail>findByActivityId(@Param("activityId") String activityId);
}
