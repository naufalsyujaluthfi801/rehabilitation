package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MTaskRepository extends JpaRepository <MTask, String> {
    @Query(value = "SELECT m FROM MTask m WHERE m.date =:date")
    List<MTask> findByTaskDate (Date date);

    @Query(value = "SELECT m FROM MTask m WHERE m.date = :date AND m.isActive = true AND m.isDeleted = false ORDER BY m.createdDate desc ")
    List<MTask> findAllByIsActiveAndDeletedTask(@Param("date") Date date);

    @Query(value = "SELECT m FROM MTask m WHERE m.date = :date AND m.id IN :taskIds AND m.isActive = true AND m.isDeleted = false ORDER BY m.createdDate desc ")
    List<MTask> findAllByTasklIds(@Param("taskIds") List<String> taskIds, @Param("date") Date date);
}
