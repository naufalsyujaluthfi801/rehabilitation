package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MActivity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MActivityRepository extends JpaRepository <MActivity, String>{
    @Query(value = "SELECT m FROM MActivity m WHERE m.date =:date ")
    List<MActivity> findByActivityDate (Date date);

    @Query(value = "SELECT m FROM MActivity m WHERE m.date = :date AND m.isActive = true AND m.isDeleted = false AND m.createdBy = :userId order by m.createdDate desc")
    List<MActivity> findAllByIsActiveAndDeletedActivity(@Param("date") Date date, @Param("userId") String userId);

    @Query(value = "SELECT m FROM MActivity m WHERE m.createdBy IN :userIds AND m.date = :date AND m.isActive = true AND m.isDeleted = false order by m.createdBy ASC, m.createdDate desc")
    List<MActivity> findAllByUserIdsPatient(@Param("userIds") List<String> userIds, @Param("date") Date date);

}
