package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.TMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TMaterialRepository extends JpaRepository <TMaterial, String> {
    @Query(value = "SELECT m FROM TMaterial m WHERE m.counselorId =:counselorId AND m.isActive = true AND m.isDeleted = false")
    List<TMaterial> findAllByCounselorId(String counselorId);

    @Query(value = "SELECT m FROM TMaterial m WHERE m.material.id =:materialId AND m.isActive = true  and m.isDeleted= false ORDER BY m.createdDate desc ")
    List<TMaterial> findAllByMaterialId(String materialId);
}
