package com.bnn.rehabilitation.repository;

import com.bnn.rehabilitation.model.MPermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MPermissionRepository extends JpaRepository<MPermission, String> {
}
