package com.bnn.rehabilitation.constant;

public class CommonConstant {
    public static final int FAILED_RESPONSE_CODE = 0;
    public static final int SUCCESS_RESPONSE_CODE = 1;

    public static final String FAILED_RESPONSE_MESSAGE = "Failed";
    public static final String SUCCESS_RESPONSE_MESSAGE = "Success";

    public static final boolean FALSE_MESSAGE = false;
    public static final boolean TRUE_MESSAGE = true;

    public static final String EXCEPTION = "EXCEPTION: ";

    public static final String ROLE_INTERNAL_TYPE = "INTERNAL";
    public static final String ROLE_EXTERNAL_TYPE = "EXTERNAL";

    public static final String FEMALE_CODE = "F";
    public static final String MALE_CODE = "M";

    public static final String ROLE_CODE_COUNSELOR = "CONS";
    public static final String ROLE_CODE_ASSISTANT_COUNSELOR = "ASSC";
    public static final String ROLE_CODE_INPATIENT = "INP";
    public static final String ROLE_CODE_OUTPATIENT = "OUTP";



}
