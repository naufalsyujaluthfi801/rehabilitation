package com.bnn.rehabilitation.constant;

public class MessageConstant {

    public static final String COMMON_MESSAGES_PATH = "messages";

    /** {0} registered successful. */
    public static final String COMMON_MESSAGE_0001 = "common.message.0001";
    /** {0} modified successful. */
    public static final String COMMON_MESSAGE_0002 = "common.message.0002";
    /** {0} delete successful. */
    public static final String COMMON_MESSAGE_0003 = "common.message.0003";
    /** {0} registered unsuccessful. */
    public static final String COMMON_MESSAGE_0004 = "common.message.0004";
    /** {0} modified unsuccessful. */
    public static final String COMMON_MESSAGE_0005 = "common.message.0005";
    /** {0} delete unsuccessful. */
    public static final String COMMON_MESSAGE_0006 = "common.message.0006";
    /** {0} confirmation message. */
    public static final String COMMON_MESSAGE_0007 = "common.message.0007";
    /** {0} Are you sure want to {0} this {1}? */
    public static final String COMMON_MESSAGE_0008 = "common.message.0008";

    /** {0} doest not exist. */
    public static final String ERROR_MESSAGE_0001 = "error.message.0001";
    /** {0} required. */
    public static final String ERROR_MESSAGE_0002 = "error.message.0002";
    /** {0} not found. */
    public static final String ERROR_MESSAGE_0003 = "error.message.0003";
    /** {0} unhandled error. */
    public static final String ERROR_MESSAGE_0004 = "error.message.0004";
    /** {0} password does not match. */
    public static final String ERROR_MESSAGE_0005 = "error.message.0005";
    /** {0} old password not match. */
    public static final String ERROR_MESSAGE_0006 = "error.message.0006";
    /** {0} New Password and Re-Type Password doesn't match. */
    public static final String ERROR_MESSAGE_0007 = "error.message.0007";
    /** {0} Account id not match with token. */
    public static final String ERROR_MESSAGE_0008 = "error.message.0008";

    /** Name is required. */
    public static final String VALIDATION_MESSAGE_0001 = "{validation.message.0001}";
    /** Username is required. */
    public static final String VALIDATION_MESSAGE_0002 = "validation.message.0002";
    /** Password is required. */
    public static final String VALIDATION_MESSAGE_0003 = "validation.message.0003";
    /** Gender is required. */
    public static final String VALIDATION_MESSAGE_0004 = "validation.message.0004";
    /** Invalid email address. */
    public static final String VALIDATION_MESSAGE_0005 = "{validation.message.0005}";
    /** Email address is already used. */
    public static final String VALIDATION_MESSAGE_0006 = "{validation.message.0006}";
    /** Old Password is required. */
    public static final String VALIDATION_MESSAGE_0007 = "{validation.message.0007}";
    /** New Password is required. */
    public static final String VALIDATION_MESSAGE_0008 = "{validation.message.0008}";
    /** Re-type Password is required. */
    public static final String VALIDATION_MESSAGE_0009 = "{validation.message.0009}";
    /** NIK is required. */
    public static final String VALIDATION_MESSAGE_0010 = "validation.message.0010";
    /** Username is required. */
    public static final String VALIDATION_MESSAGE_0011 = "validation.message.0011";
    /** Start time activity is required. */
    public static final String VALIDATION_MESSAGE_0012 = "validation.message.0012";
    /** End time activity is required. */
    public static final String VALIDATION_MESSAGE_0013 = "validation.message.0013";
    /** Description activity is required. */
    public static final String VALIDATION_MESSAGE_0014 = "validation.message.0014";
    /** NIK must be 16 characters. */
    public static final String VALIDATION_MESSAGE_0015 = "validation.message.0015";
    /** Password must be longer than 8 characters. */
    public static final String VALIDATION_MESSAGE_0016 = "validation.message.0016";
    /** Fullname is required. */
    public static final String VALIDATION_MESSAGE_0017 = "validation.message.0017";
    /** Role is required. */
    public static final String VALIDATION_MESSAGE_0018 = "validation.message.0018";
    /** Address is required. */
    public static final String VALIDATION_MESSAGE_0019 = "validation.message.0019";
    /** Counselor is required. */
    public static final String VALIDATION_MESSAGE_0020 = "validation.message.0020";


    /** Success Logout. */
    public static final String SUCCESS_LOGOUT = "success logout";


    /** Search */
    public static final String COMMON_PAGE_0001 = "common.page.0001";
    /** Register */
    public static final String COMMON_PAGE_0002 = "common.page.0002";
    /** Delete */
    public static final String COMMON_PAGE_0003 = "common.page.0003";
    /** Modify */
    public static final String COMMON_PAGE_0004 = "common.page.0004";
    /** Save */
    public static final String COMMON_PAGE_0005 = "common.page.0005";
    /** Back */
    public static final String COMMON_PAGE_0006 = "common.page.0006";
    /** # */
    public static final String COMMON_PAGE_0007 = "common.page.0007";
    /** Created Data */
    public static final String COMMON_PAGE_0008 = "common.page.0008";
    /** Updated Data */
    public static final String COMMON_PAGE_0009 = "common.page.0009";
    /** Active */
    public static final String COMMON_PAGE_0010 = "common.page.0010";
    /** Action */
    public static final String COMMON_PAGE_0011 = "common.page.0011";
    /** Is active */
    public static final String COMMON_PAGE_0012 = "common.page.0012";
    /** Yes */
    public static final String COMMON_PAGE_0013 = "common.page.0013";
    /** No */
    public static final String COMMON_PAGE_0014 = "common.page.0014";
    /** Male */
    public static final String COMMON_PAGE_0015 = "common.page.0015";
    /** Female */
    public static final String COMMON_PAGE_0016 = "common.page.0016";
    /** Account */
    public static final String COMMON_PAGE_0017 = "common.page.0017";
    /** Please select one */
    public static final String COMMON_PAGE_0018 = "common.page.0018";
    /** Email */
    public static final String COMMON_PAGE_0019 = "common.page.0019";
    /** Date of birth */
    public static final String COMMON_PAGE_0020 = "common.page.0020";
    /** Place of birth */
    public static final String COMMON_PAGE_0021 = "common.page.0021";
    /** Gender */
    public static final String COMMON_PAGE_0022 = "common.page.0022";
    /** Submit */
    public static final String COMMON_PAGE_0023 = "common.page.0023";
    /** View */
    public static final String COMMON_PAGE_0024 = "common.page.0024";
    /** Download */
    public static final String COMMON_PAGE_0025 = "common.page.0025";
    /** Password */
    public static final String COMMON_PAGE_0026 = "common.page.0026";

    /** User */
    public static final String USER_PAGE_0001 = "user.page.0001";

    /** Full Name */
    public static final String PATIENT_PAGE_0001 = "patient.page.0001";
    /** Username */
    public static final String PATIENT_PAGE_0002 = "patient.page.0002";
    /** NIK*/
    public static final String PATIENT_PAGE_0003 = "patient.page.0003";
    /** PATIEN ACCOUNT LIST*/
    public static final String PATIENT_PAGE_0004 = "patient.page.0004";
    /** Enter NIK*/
    public static final String PATIENT_PAGE_0005 = "patient.page.0005";
    /** ENTER FULL NAME*/
    public static final String PATIENT_PAGE_0006 = "patient.page.0006";
    /** ENTER USERNAME*/
    public static final String PATIENT_PAGE_0007 = "patient.page.0007";
    /** ENTER PASSWORD*/
    public static final String PATIENT_PAGE_0008 = "patient.page.0008";
    /** ENTER PLACE OF BIRTH*/
    public static final String PATIENT_PAGE_0009 = "patient.page.0009";
    /** ENTER ADDRESS*/
    public static final String PATIENT_PAGE_0010 = "patient.page.0010";
    /** ADDRESS*/
    public static final String PATIENT_PAGE_0011 = "patient.page.0011";
    /** ROLE PASIEN*/
    public static final String PATIENT_PAGE_0012 = "patient.page.0012";
    /** COUNSELOR*/
    public static final String PATIENT_PAGE_0013 = "patient.page.0013";
    /** Old Username*/
    public static final String PATIENT_PAGE_0014 = "patient.page.0014";
    /** New Username*/
    public static final String PATIENT_PAGE_0015 = "patient.page.0015";
    /** Date Of Birth*/
    public static final String PATIENT_PAGE_0016 = "patient.page.0016";
    /** Place Of Birth*/
    public static final String PATIENT_PAGE_0017 = "patient.page.0017";
    /** Gender*/
    public static final String PATIENT_PAGE_0018 = "patient.page.0018";
    /** Submit*/
    public static final String PATIENT_PAGE_0019 = "patient.page.0019";

    /** FIND YOUR DATA*/
    public static final String ACTIVITY_PAGE_0001 = "activity.page.0001";
    /** ENTER THE DATE*/
    public static final String ACTIVITY_PAGE_0002 = "activity.page.0002";
    /** YOUR ACTIVITY LIST*/
    public static final String ACTIVITY_PAGE_0003 = "activity.page.0003";
    /** PATIENT NAME*/
    public static final String ACTIVITY_PAGE_0004 = "activity.page.0004";
    /** START TIME ACTIVITY*/
    public static final String ACTIVITY_PAGE_0005 = "activity.page.0005";
    /** END TIME ACTIVITY*/
    public static final String ACTIVITY_PAGE_0006 = "activity.page.0006";
    /** ACTIVITY DESCRIPTION*/
    public static final String ACTIVITY_PAGE_0007 = "activity.page.0007";
    /** EVIDENCE OF ACTIVITY*/
    public static final String ACTIVITY_PAGE_0008 = "activity.page.0008";
    /** Enter Start Time Activity*/
    public static final String ACTIVITY_PAGE_0009 = "activity.page.0009";
    /** Enter End Time Activity*/
    public static final String ACTIVITY_PAGE_0010 = "activity.page.0010";
    /** Enter Activity Description*/
    public static final String ACTIVITY_PAGE_0011 = "activity.page.0011";
    /** Upload Your Activity File Below*/
    public static final String ACTIVITY_PAGE_0012 = "activity.page.0012";
    /** Back*/
    public static final String ACTIVITY_PAGE_0013 = "activity.page.0013";
    /** Submit*/
    public static final String ACTIVITY_PAGE_0014 = "activity.page.0014";
    /** Add*/
    public static final String ACTIVITY_PAGE_0015 = "activity.page.0015";
    /** Comments*/
    public static final String ACTIVITY_PAGE_0016 = "activity.page.0016";



    /** Your Material List*/
    public static final String MATERIAL_PAGE_0001 = "material.page.0001";
    /** Documentation*/
    public static final String MATERIAL_PAGE_0002 = "material.page.0002";
    /** File*/
    public static final String MATERIAL_PAGE_0003 = "material.page.0003";
    /** Action*/
    public static final String MATERIAL_PAGE_0004 = "material.page.0004";
    /** Enter Your Description*/
    public static final String MATERIAL_PAGE_0005 = "material.page.0005";
    /** Upload Your Material File Below*/
    public static final String MATERIAL_PAGE_0006 = "material.page.0006";
    /** Input Activities*/
    public static final String MATERIAL_PAGE_0007 = "material.page.0007";
    /** Submit*/
    public static final String MATERIAL_PAGE_0008 = "material.page.0008";

    /** Your Task List*/
    public static final String TASK_PAGE_0001 = "task.page.0001";
    /** Answer*/
    public static final String TASK_PAGE_0002 = "task.page.0002";
    /** Task Collection*/
    public static final String TASK_PAGE_0003 = "task.page.0003";
    /** Checked*/
    public static final String TASK_PAGE_0004 = "task.page.0004";
    /** Completed Task List*/
    public static final String TASK_PAGE_0005 = "task.page.0005";
    /** Patient's Name*/
    public static final String TASK_PAGE_0006 = "task.page.0006";
    /** Uploaded Files*/
    public static final String TASK_PAGE_0007 = "task.page.0007";
    /** Back*/
    public static final String TASK_PAGE_0008 = "task.page.0008";
    /** Submit*/
    public static final String TASK_PAGE_0009 = "task.page.0009";
    /** Upload Files Below*/
    public static final String TASK_PAGE_0010 = "task.page.0010";


















}
