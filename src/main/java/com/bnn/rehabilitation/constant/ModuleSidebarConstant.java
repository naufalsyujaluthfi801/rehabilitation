package com.bnn.rehabilitation.constant;

public class ModuleSidebarConstant {
    /** Dashboard */
    public static final String SIDEBAR_0001 = "sidebar.0001";

    /** Activity */
    public static final String SIDEBAR_0002 = "sidebar.0002";

    /** Register Activity */
    public static final String SIDEBAR_0003 = "sidebar.0003";

    /** Material And Task */
    public static final String SIDEBAR_0004 = "sidebar.0004";

    /** Material */
    public static final String SIDEBAR_0005 = "sidebar.0005";

    /** Task */
    public static final String SIDEBAR_0006 = "sidebar.0006";

    /** InputTask */
    public static final String SIDEBAR_0007 = "sidebar.0007";

    /** InputUserPatient */
    public static final String SIDEBAR_0008 = "sidebar.0008";

    /** Patient */
    public static final String SIDEBAR_0009 = "sidebar.0009";

    /** Register Patient */
    public static final String SIDEBAR_0010 = "sidebar.0010";
}
