package com.bnn.rehabilitation.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "m_task_patient_detail")
@Entity
public class MTaskPatientDetail {
    @Id
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "task_patient_id", nullable = false)
    private MTaskPatient taskPatient;

    @ManyToOne(optional = false)
    @JoinColumn(name = "upload_id", nullable = false)
    private MUpload upload;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive = false;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = false;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;
}