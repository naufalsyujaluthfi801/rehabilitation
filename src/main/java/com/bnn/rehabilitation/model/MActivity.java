package com.bnn.rehabilitation.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Date;

@Data
@NoArgsConstructor
@Table(name = "m_activity")
@Entity
public class MActivity {
    @Id
    @Size(max = 50)
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @Size(max = 50)
    @NotNull
    @Column(name = "start_time", nullable = false, length = 50)
    private String startTime;

    @Size(max = 50)
    @NotNull
    @Column(name = "end_time", nullable = false, length = 50)
    private String endTime;

    @NotNull
    @Column(name = "description_activity", nullable = false)
    private String descriptionActivity;

    @NotNull
    @Column(name = "date", nullable = false)
    private Date date;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive = true;

    @NotNull
    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = false;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Size(max = 50)
    @Column(name = "updated_by", length = 50)
    private String updatedBy;
}
