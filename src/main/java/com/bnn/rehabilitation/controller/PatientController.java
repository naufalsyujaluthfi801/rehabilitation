package com.bnn.rehabilitation.controller;

import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.form.ActivityInputForm;
import com.bnn.rehabilitation.form.ActivitySearchForm;
import com.bnn.rehabilitation.form.PatientInputForm;
import com.bnn.rehabilitation.service.MUserService;
import com.bnn.rehabilitation.validator.RegisterPatientValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("patient")
@SessionAttributes(value = {"roleOptions", "genderOptions", "counselorOptions"})
public class PatientController {

    private final MUserService mUserService;
    private final RegisterPatientValidator registerPatientValidator;

    public PatientController(MUserService mUserService, RegisterPatientValidator registerPatientValidator) {
        this.mUserService = mUserService;
        this.registerPatientValidator = registerPatientValidator;
    }

    @ModelAttribute
    public PatientInputForm setupPatientInputForm(){
        return new PatientInputForm();
    }

    @GetMapping("register")
    public String getRegisterPatient (@ModelAttribute PatientInputForm patientInputForm, Model model){
        LoadRegisterUserPatientRequest request = new LoadRegisterUserPatientRequest();
        LoadRegisterUserPatientResponse response = this.mUserService.loadUserPatient(request);
        model.addAttribute("roleOptions", response.getRoleDtoList());
        model.addAttribute("genderOptions", response.getGenderDtoList());
        model.addAttribute("counselorOptions", response.getUserDtoList());
        return "patient/registerPatient";
    }

    @PostMapping("register")
    public String proccessRegisterUserPatientPage(@ModelAttribute @Validated PatientInputForm patientInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session){
        registerPatientValidator.validate(patientInputForm,result);
        if (result.hasErrors()){
            return "patient/registerPatient";
        }else {
            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
            RegisterUserPatientRequest request = new RegisterUserPatientRequest();
            request.setUserSession(userSessionDto);
            request.setNik(patientInputForm.getNik());
            request.setFullName(patientInputForm.getFullName());
            request.setUserName(patientInputForm.getUserName());
            request.setPassword(patientInputForm.getPassword());
            request.setDateOfBirth(patientInputForm.getDateOfBirth());
            request.setPlaceOfBirth(patientInputForm.getPlaceOfBirth());
            request.setAddress(patientInputForm.getAddress());
            request.setGender(patientInputForm.getGender());
            request.setRoleId(patientInputForm.getRoleId());
            request.setCounselorId(patientInputForm.getCounselorId());
            RegisterUserPatientResponse response = this.mUserService.insertUserPatient(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "patient/registerPatient";
            } else {
                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                attributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/dashboard";
            }
        }
    }

    @PostMapping("modifyUserPatient")
    public String proccessModifyUser(@ModelAttribute @Valid PatientInputForm patientInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session){
        if (result.hasErrors()) {
            return "patient/modifyUserPatient";
        } else {
            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
            ModifyUserPatientRequest request = new ModifyUserPatientRequest();
            request.setUserSession(userSessionDto);
            request.setId(patientInputForm.getId());
            request.setNik(patientInputForm.getNik());
            request.setFullName(patientInputForm.getFullName());
            request.setUserName(patientInputForm.getUserName());
            request.setNewUsername(patientInputForm.getNewUsername());
            request.setPassword(patientInputForm.getPassword());
            request.setDateOfBirth(patientInputForm.getDateOfBirth());
            request.setPlaceOfBirth(patientInputForm.getPlaceOfBirth());
            request.setAddress(patientInputForm.getAddress());
            request.setGender(patientInputForm.getGender());
            ModifyUserPatientResponse response = this.mUserService.modifyUserPatient(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "patient/modifyUserPatient";
            } else {
                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                attributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/logout";
            }
        }
    }

    @GetMapping("modifyUserPatient")
    public String getModifyPatientUser(Model model, RedirectAttributes attributes, HttpSession session) {
        UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
        LoadModifyUserPatientRequest request = new LoadModifyUserPatientRequest();
        request.setUserSessionDto(userSessionDto);
        LoadModifyUserPatientResponse response =  this.mUserService.loadModifyUserPatient(request);
        if (!response.getIsSuccess()){
            attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
            attributes.addFlashAttribute("message", response.getMessage());
            return "redirect:/patient";
        }
        model.addAttribute("data", response.getData());
        model.addAttribute("genderOptions",response.getGenderDtoList());
        return "patient/modifyUserPatient";
    }

    @GetMapping("")
    public String getViewPatient(Model model) {
        ViewPatientRequest request = new ViewPatientRequest();
        ViewPatientResponse viewPatientResponse = this.mUserService.viewPatient(request);
        if (!viewPatientResponse.getIsSuccess()) {
            model.addAttribute("message",viewPatientResponse.getMessage());
            model.addAttribute("isSuccess",viewPatientResponse.getIsSuccess());
        }else {
            model.addAttribute("data",viewPatientResponse.getData());
        }
        return "patient/viewPatient";
    }

    @PostMapping("delete")
    public String deletePatient(PatientInputForm patientInputForm, RedirectAttributes redirectAttributes, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        DeletePatientRequest request = new DeletePatientRequest();
        request.setId(patientInputForm.getId());
        request.setUserSession(userSession);
        DeletePatientResponse response = this.mUserService.deletePatient(request);
        redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
        redirectAttributes.addFlashAttribute("message", response.getMessage());
        return "redirect:/patient";
    }
}
