package com.bnn.rehabilitation.controller;

import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.form.*;
import com.bnn.rehabilitation.service.MTaskPatientService;
import com.bnn.rehabilitation.service.MTaskService;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("task")
@SessionAttributes(types = TaskSearchForm.class)
public class TaskController {

    private final MTaskService mTaskService;
    private final MTaskPatientService mTaskPatientService;

    public TaskController(MTaskService mTaskService, MTaskPatientService mTaskPatientService) {
        this.mTaskService = mTaskService;
        this.mTaskPatientService = mTaskPatientService;
    }

    @ModelAttribute
    public TaskInputForm setupTaskInputForm() {
        return new TaskInputForm();
    }

    @ModelAttribute
    public TaskSearchForm setupTaskSearchForm() {
        return new TaskSearchForm();
    }



    @GetMapping("")
    public String getViewTask(@ModelAttribute TaskSearchForm taskSearchForm ,Model model, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        ViewTaskRequest request = new ViewTaskRequest();
        request.setDate(taskSearchForm.getDate());
        request.setUserSession(userSession);
        ViewTaskResponse viewTaskResponse = this.mTaskService.viewTask(request);
        if (!viewTaskResponse.getIsSuccess()) {
            model.addAttribute("message",viewTaskResponse.getMessage());
            model.addAttribute("isSuccess",viewTaskResponse.getIsSuccess());
        }else {
            model.addAttribute("data",viewTaskResponse.getData());
        }
        return "task/indexTask";
    }

    @PostMapping
    public String processSearchTask(TaskSearchForm taskSearchForm){
        return "redirect:/task";
    }

    @GetMapping("registerTask")
    public String getTask(@ModelAttribute TaskInputForm taskInputForm, Model model) {
        LoadRegisterTaskRequest request = new LoadRegisterTaskRequest();
        LoadRegisterTaskResponse response = this.mTaskService.loadTask(request);
        model.addAttribute("counselorOptions", response.getUserDtoList());
        return "task/registerTask";
    }

    @PostMapping("registerTask")
    public String processRegisterTaskPage(@ModelAttribute @Valid TaskInputForm taskInputForm, BindingResult result, Model model, RedirectAttributes redirectAttributes, HttpSession session){
        if (result.hasErrors()) {
            return "task/registerTask";
        }else {
            UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
            RegisterTaskRequest request = new RegisterTaskRequest();
            request.setUserSession(userSession);
            request.setDescriptionTask(taskInputForm.getDescriptionTask());
            request.setFiles(taskInputForm.getFiles());
            request.setCounselorIds(taskInputForm.getCounselorIds());
            RegisterTaskResponse response = this.mTaskService.insertTask(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "task/registerTask";
            } else {
                redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                redirectAttributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/task";
            }
        }
    }

//    @PostMapping("modify")
//    public String proccessModifyTask(@ModelAttribute @Valid TaskInputForm taskInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session) {
//        if (result.hasErrors()) {
//            return "task/modifyTask";
//        } else {
//            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
//            ModifyTaskRequest request = new ModifyTaskRequest();
//            request.setId(taskInputForm.getId());
//            request.setUserSession(userSessionDto);
//            request.setDescriptionTask(taskInputForm.getDescriptionTask());
//            request.setFiles(taskInputForm.getFiles());
//            request.setTaskDetailDtos(taskInputForm.getTaskDetailDtos());
//            ModifyTaskResponse response = this.mTaskService.modifyTask(request);
//            if (!response.getIsSuccess()) {
//                model.addAttribute("isSuccess", response.getIsSuccess());
//                model.addAttribute("message", response.getMessage());
//                return "task/modifyTask";
//            } else {
//                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
//                attributes.addFlashAttribute("message", response.getMessage());
//                return "redirect:/task";
//            }
//        }
//    }

    @PostMapping("modify-patient")
    public String proccessModifyPatientTask(@ModelAttribute @Valid TaskInputForm taskInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session) {
        if (result.hasErrors()) {
            return "task/modifyTask";
        } else {
            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
            ModifyTaskPatientRequest request = new ModifyTaskPatientRequest();
            request.setId(taskInputForm.getId());
            request.setUserSession(userSessionDto);
            request.setDescriptionTask(taskInputForm.getDescriptionTask());
            request.setFiles(taskInputForm.getFiles());
            request.setTaskPatientDetailDtos(taskInputForm.getTaskPatientDetailDtos());
            ModifyTaskPatientResponse response = this.mTaskPatientService.modifyTaskPatient(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "task/modifyTask";
            } else {
                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                attributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/task";
            }
        }
    }

//    @PostMapping("checked")
//    public String proccessModifyTask(@ModelAttribute @Valid TaskInputForm taskInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session) {
//        if (result.hasErrors()) {
//            return "task/modifyTask";
//        } else {
//            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
//            ModifyTaskRequest request = new ModifyTaskRequest();
//            request.setId(taskInputForm.getId());
//            request.setUserSession(userSessionDto);
//            request.setDescriptionTask(taskInputForm.getDescriptionTask());
//            request.setFiles(taskInputForm.getFiles());
//            request.setTaskDetailDtos(taskInputForm.getTaskDetailDtos());
//            ModifyTaskResponse response = this.mTaskService.modifyTask(request);
//            if (!response.getIsSuccess()) {
//                model.addAttribute("isSuccess", response.getIsSuccess());
//                model.addAttribute("message", response.getMessage());
//                return "task/modifyTask";
//            } else {
//                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
//                attributes.addFlashAttribute("message", response.getMessage());
//                return "redirect:/task";
//            }
//        }
//    }

    @GetMapping("modify-patient/{id}")
    public String getModifyPatientTask(@PathVariable("id") String id, Model model, RedirectAttributes attributes, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        LoadModifyTaskPatientRequest request = new LoadModifyTaskPatientRequest();
        request.setId(id);
        request.setUserSession(userSession);
        LoadModifyTaskPatientResponse response =  this.mTaskPatientService.loadModifyTaskPatient(request);
        if (!response.getIsSuccess()){
            attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
            attributes.addFlashAttribute("message", response.getMessage());
            return "redirect:/task";
        }
        model.addAttribute("data", response.getData());
        return "task/modifyTask";
    }

    @PostMapping("delete")
    public String deleteTask(TaskInputForm taskInputForm, RedirectAttributes redirectAttributes, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        DeleteTaskRequest request = new DeleteTaskRequest();
        request.setId(taskInputForm.getId());
        request.setUserSession(userSession);
        DeleteTaskResponse response = this.mTaskService.deleteTask(request);
        redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
        redirectAttributes.addFlashAttribute("message", response.getMessage());
        return "redirect:/task";
    }

    @GetMapping("checked/{id}")
    public String getViewTaskPatient(@PathVariable("id") String id, @ModelAttribute TaskSearchForm taskSearchForm , Model model, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        ViewTaskPatientRequest request = new ViewTaskPatientRequest();
        request.setId(id);
        request.setDate(taskSearchForm.getDate());
        request.setUserSession(userSession);
        ViewTaskPatientResponse response= this.mTaskService.viewTaskPatient(request);
        if (!response.getIsSuccess()) {
            model.addAttribute("message",response.getMessage());
            model.addAttribute("isSuccess",response.getIsSuccess());
        }else {
            model.addAttribute("data",response.getData());
        }
        return "task/checkedTask";
    }
}
