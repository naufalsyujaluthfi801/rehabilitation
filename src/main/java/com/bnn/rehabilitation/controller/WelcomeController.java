package com.bnn.rehabilitation.controller;

import com.bnn.rehabilitation.dto.request.LoadSignupRequest;
import com.bnn.rehabilitation.dto.request.SignupRequest;
import com.bnn.rehabilitation.dto.response.LoadSignupResponse;
import com.bnn.rehabilitation.dto.response.SignupResponse;
import com.bnn.rehabilitation.form.SignupForm;
import com.bnn.rehabilitation.helper.AuthenticationHelper;
import com.bnn.rehabilitation.service.MUserService;
import com.bnn.rehabilitation.validator.RegisterCounselorValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping
@SessionAttributes(value = {"data"})
public class WelcomeController {

    private final MUserService mUserService;
    private final AuthenticationHelper authenticationHelper;
    private final RegisterCounselorValidator registerCounselorValidator;

    public WelcomeController(MUserService mUserService, AuthenticationHelper authenticationHelper, RegisterCounselorValidator registerCounselorValidator) {
        this.mUserService = mUserService;
        this.authenticationHelper = authenticationHelper;
        this.registerCounselorValidator = registerCounselorValidator;
    }

    @ModelAttribute
    public SignupForm setupSignupForm() {
        return new SignupForm();
    }

    @GetMapping("/login")
    public String loadLoginPage() {
        boolean isAuthenticated = this.authenticationHelper.isAuthenticated();
        if (isAuthenticated) {
            return "redirect:/dashboard";
        } else {
            return "welcome/login";
        }
    }

    @GetMapping("/signup")
    public String loadSignupPage(Model model) {
        boolean isAuthenticated = this.authenticationHelper.isAuthenticated();
        if (isAuthenticated) {
            return "redirect:/";
        } else {
            LoadSignupRequest request = new LoadSignupRequest();
            LoadSignupResponse response = this.mUserService.loadSignup(request);
            model.addAttribute("data",response.getRoleDtolist());
            return "welcome/signup";
        }
    }

    @PostMapping("/signup")
    public String submitSignUp(@Validated @ModelAttribute SignupForm signupForm, BindingResult result){
        registerCounselorValidator.validate(signupForm, result);
        if (result.hasErrors()){
            return "welcome/signup";
        }else {
            SignupRequest request = new SignupRequest(signupForm);
            SignupResponse response = this.mUserService.insertUser(request);
            return "redirect:/login";
        }
    }

}
