package com.bnn.rehabilitation.controller;

import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.form.ActivityInputForm;
import com.bnn.rehabilitation.form.ActivitySearchForm;
import com.bnn.rehabilitation.service.MActivityService;
import com.bnn.rehabilitation.validator.RegisterActivityValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("activity")
@SessionAttributes(types = ActivitySearchForm.class)
public class ActivityController {

    private final MActivityService mActivityService;
    private final RegisterActivityValidator registerActivityValidator;

    public ActivityController(MActivityService mActivityService, RegisterActivityValidator registerActivityValidator) {
        this.mActivityService = mActivityService;
        this.registerActivityValidator = registerActivityValidator;
    }

    @ModelAttribute
    public ActivityInputForm setupActivityInputForm(){
        return new ActivityInputForm();
    }

    @ModelAttribute
    public ActivitySearchForm setupActivitySearchForm(){
        return new ActivitySearchForm();
    }

    @GetMapping("")
    public String getViewActivity(@ModelAttribute ActivitySearchForm activitySearchForm, Model model, HttpSession session) {
        UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
        ViewActivityRequest request = new ViewActivityRequest();
        request.setDate(activitySearchForm.getDate());
        request.setUserSession(userSessionDto) ;
        ViewActivityResponse viewActivityResponse = this.mActivityService.viewActivity(request);
        if (!viewActivityResponse.getIsSuccess()) {
            model.addAttribute("message",viewActivityResponse.getMessage());
            model.addAttribute("isSuccess",viewActivityResponse.getIsSuccess());
        }else {
            model.addAttribute("data",viewActivityResponse.getData());
        }
        return "activity/viewActivity";
    }

    @PostMapping
    public String processSearchActivity(ActivitySearchForm activitySearchForm){
        return "redirect:/activity";
    }

    @GetMapping("register")
    public String getRegisterActivity() {
        return "activity/register";
    }

    @PostMapping("register")
    public String processRegisterActivityPage(@ModelAttribute @Valid ActivityInputForm activityInputForm, BindingResult result, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        registerActivityValidator.validate(activityInputForm,result);
        if (result.hasErrors()) {
            return "activity/register";
        } else {
            UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
            RegisterActivityRequest request = new RegisterActivityRequest();
            request.setUserSession(userSession);
            request.setStartTime(activityInputForm.getStartTime());
            request.setEndTime(activityInputForm.getEndTime());
            request.setDescriptionActivity(activityInputForm.getDescriptionActivity());
            request.setFiles(activityInputForm.getFiles());
            RegisterActivityResponse response = this.mActivityService.insertActivity(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "activity/register";
            } else {
                redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                redirectAttributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/activity";
            }
        }
    }

    @PostMapping("modifyActivity")
    public String proccessModifyActivity(@ModelAttribute @Valid ActivityInputForm activityInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session){
        if (result.hasErrors()) {
            return "activity/modifyActivity";
        } else {
            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
            ModifyActivityRequest request = new ModifyActivityRequest();
            request.setId(activityInputForm.getId());
            request.setUserSession(userSessionDto);
            request.setStarTime(activityInputForm.getStartTime());
            request.setEndTime(activityInputForm.getEndTime());
            request.setDescriptionActivity(activityInputForm.getDescriptionActivity());
            request.setFiles(activityInputForm.getFiles());
            request.setActivityDetailDtos(activityInputForm.getActivityDetailDtos());
            ModifyActivityResponse response = this.mActivityService.modifyActivity(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "activity/modifyActivity";
            } else {
                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                attributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/activity";
            }
        }
    }

    @GetMapping("modifyActivity/{id}")
    public String getModifyActivity(@PathVariable("id") String id, Model model, RedirectAttributes attributes) {
        LoadModifyActivityRequest request = new LoadModifyActivityRequest(id);
        LoadModifyActivityResponse response =  this.mActivityService.loadModify(request);
        if (!response.getIsSuccess()){
            attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
            attributes.addFlashAttribute("message", response.getMessage());
            return "redirect:/activity";
        }
        model.addAttribute("data", response.getData());
        return "activity/modifyActivity";
    }

    @PostMapping("delete")
    public String deleteActivity(ActivityInputForm activityInputForm, RedirectAttributes redirectAttributes, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        DeleteActivityRequest request = new DeleteActivityRequest();
        request.setId(activityInputForm.getId());
        request.setUserSession(userSession);
        DeleteActivityResponse response = this.mActivityService.deleteActivity(request);
        redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
        redirectAttributes.addFlashAttribute("message", response.getMessage());
        return "redirect:/activity";
    }

}
