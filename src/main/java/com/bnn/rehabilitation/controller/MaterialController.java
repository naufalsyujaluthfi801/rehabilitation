package com.bnn.rehabilitation.controller;

import com.bnn.rehabilitation.dto.UserSessionDto;
import com.bnn.rehabilitation.dto.request.*;
import com.bnn.rehabilitation.dto.response.*;
import com.bnn.rehabilitation.form.ActivityInputForm;
import com.bnn.rehabilitation.form.MaterialInputForm;
import com.bnn.rehabilitation.form.MaterialSearchForm;
import com.bnn.rehabilitation.service.MMaterialService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("material")
@SessionAttributes(types = MaterialSearchForm.class)
public class MaterialController {

    private final MMaterialService mMaterialService;

    public MaterialController(MMaterialService mMaterialService) {
        this.mMaterialService = mMaterialService;
    }

    @ModelAttribute
    public MaterialInputForm setupMaterialInputForm() {
        return new MaterialInputForm();
    }

    @ModelAttribute
    public MaterialSearchForm setupMaterialSearchForm(){
        return new MaterialSearchForm();
    }

    @GetMapping("")
    public String getViewMaterial(@ModelAttribute MaterialSearchForm materialSearchForm, Model model, HttpSession session) {
        UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
        ViewMaterialRequest request = new ViewMaterialRequest();
        request.setDate(materialSearchForm.getDate());
        request.setUserSession(userSessionDto);
        ViewMaterialResponse viewMaterialResponse = this.mMaterialService.viewMaterial(request);
        if (!viewMaterialResponse.getIsSuccess()) {
            model.addAttribute("message",viewMaterialResponse.getMessage());
            model.addAttribute("isSuccess",viewMaterialResponse.getIsSuccess());
        }else {
            model.addAttribute("data",viewMaterialResponse.getData());
    }
        return "material/indexMaterial";
    }

    @PostMapping
    public String processSearchMaterial(MaterialSearchForm materialSearchForm){
        return "redirect:/material";
    }

    @GetMapping("registerMaterial")
    public String getMaterial(@ModelAttribute MaterialInputForm form, Model model) {
        LoadRegisterMaterialRequest request = new LoadRegisterMaterialRequest();
        LoadRegisterMaterialResponse response = this.mMaterialService.loadMaterial(request);
        model.addAttribute("counselorOptions", response.getUserDtoList());
        return "material/registerMaterial";
    }

    @PostMapping("registerMaterial")
    public String processRegisterMaterialPage(@ModelAttribute @Valid MaterialInputForm materialInputForm, BindingResult result, Model model, RedirectAttributes redirectAttributes, HttpSession session){
        if (result.hasErrors()) {
            return "material/registerMaterial";
        }else {
            UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
            RegisterMaterialRequest request = new RegisterMaterialRequest();
            request.setUserSession(userSession);
            request.setDescriptionMaterial(materialInputForm.getDescriptionMaterial());
            request.setFiles(materialInputForm.getFiles());
            request.setCounselorIds(materialInputForm.getCounselorIds());
            RegisterMaterialResponse response = this.mMaterialService.insertMaterial(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "material/registerMaterial";
            }else {
                redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                redirectAttributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/material";
            }
        }
    }

//    @PostMapping("modify")
//    public String proccessModifyActivity(@ModelAttribute @Valid ActivityInputForm activityInputForm, BindingResult result, Model model, RedirectAttributes attributes, HttpSession session){
//        if (result.hasErrors()) {
//            return "activity/modifyActivity";
//        } else {
//            UserSessionDto userSessionDto = (UserSessionDto) session.getAttribute("userSession");
//            ModifyActivityRequest request = new ModifyActivityRequest();
//            request.setId(activityInputForm.getId());
//            request.setUserSession(userSessionDto);
//            request.setStarTime(activityInputForm.getStartTime());
//            request.setEndTime(activityInputForm.getEndTime());
//            request.setDescriptionActivity(activityInputForm.getDescriptionActivity());
//            request.setFiles(activityInputForm.getFiles());
//            request.setActivityDetailDtos(activityInputForm.getActivityDetailDtos());
//            ModifyActivityResponse response = this.mActivityService.modifyActivity(request);
//            if (!response.getIsSuccess()) {
//                model.addAttribute("isSuccess", response.getIsSuccess());
//                model.addAttribute("message", response.getMessage());
//                return "activity/modifyActivity";
//            } else {
//                attributes.addFlashAttribute("isSuccess", response.getIsSuccess());
//                attributes.addFlashAttribute("message", response.getMessage());
//                return "redirect:/activity";
//            }
//        }
//    }

    @PostMapping("delete")
    public String deleteMaterial(MaterialInputForm materialInputForm, RedirectAttributes redirectAttributes, HttpSession session) {
        UserSessionDto userSession = (UserSessionDto) session.getAttribute("userSession");
        DeleteMaterialRequest request = new DeleteMaterialRequest();
        request.setId(materialInputForm.getId());
        request.setUserSession(userSession);
        DeleteMaterialResponse response = this.mMaterialService.deleteMaterial(request);
        redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
        redirectAttributes.addFlashAttribute("message", response.getMessage());
        return "redirect:/material";
    }
}
