package com.bnn.rehabilitation.dto;

import com.bnn.rehabilitation.model.MUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSessionDto {
    private String userId;
    private String username;
    private String roleId;
    private String roleName;
    private List<String> authorities;
    private String code;
    private String fullName;

    public UserSessionDto(MUser user, List<String> authorities, String fullName) {
        this.userId = user.getId();
        this.username = user.getUsername();
        this.roleId = user.getRole() != null ? user.getRole().getId() : null;
        this.roleName = user.getRole() != null ? user.getRole().getName() : null;
        this.authorities = authorities;
        this.code = user.getRole() != null ? user.getRole().getCode() : null;
        this.fullName = fullName;
    }
}
