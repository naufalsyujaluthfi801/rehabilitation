package com.bnn.rehabilitation.dto.request;

import com.bnn.rehabilitation.form.SignupForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignupRequest {
    private String roleId;
    private String username;
    private String password;
    private String fullName;

    public SignupRequest(SignupForm form) {
        this.roleId = form.getRoleId();
        this.username = form.getUsername();
        this.password = form.getPassword();
        this.fullName = form.getFullName();
    }
}
