package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@EqualsAndHashCode

public class RegisterTaskRequest extends GeneralRequest {
    private String descriptionTask;
    private MultipartFile[] files;
    private List<String> counselorIds;

    public RegisterTaskRequest() {

    }
}
