package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class RegisterUserPatientRequest extends GeneralRequest {
    private String nik;
    private String fullName;
    private String userName;
    private String password;
    private String dateOfBirth;
    private String placeOfBirth;
    private String address;
    private String gender;
    private String roleId;
    private String counselorId;

    public RegisterUserPatientRequest() {
    }
}
