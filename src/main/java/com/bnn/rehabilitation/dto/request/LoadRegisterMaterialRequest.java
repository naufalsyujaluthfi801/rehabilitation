package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoadRegisterMaterialRequest extends GeneralRequest{
}
