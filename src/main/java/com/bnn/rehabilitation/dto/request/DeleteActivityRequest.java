package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

@Data
@EqualsAndHashCode
public class DeleteActivityRequest extends GeneralRequest{
    private String id;
    private String startTime;
    private String endTime;
    private String descriptionActivity;
    private MultipartFile file;

    public DeleteActivityRequest() {
    }
}
