package com.bnn.rehabilitation.dto.request;

import com.bnn.rehabilitation.dto.ActivityDetailDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@EqualsAndHashCode
public class ModifyActivityRequest extends GeneralRequest {
    private String id;
    private String starTime;
    private String endTime;
    private String descriptionActivity;
    private MultipartFile[] files;
    private List<ActivityDetailDto> activityDetailDtos;

    public ModifyActivityRequest() {
    }
}
