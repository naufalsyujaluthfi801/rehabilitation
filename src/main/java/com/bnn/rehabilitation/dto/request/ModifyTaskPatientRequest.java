package com.bnn.rehabilitation.dto.request;

import com.bnn.rehabilitation.dto.TaskPatientDetailDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@EqualsAndHashCode
public class ModifyTaskPatientRequest extends GeneralRequest {
    private String id;
    private String descriptionTask;
    private MultipartFile[] files;
    private List<TaskPatientDetailDto> taskPatientDetailDtos;

    public ModifyTaskPatientRequest() {
    }
}
