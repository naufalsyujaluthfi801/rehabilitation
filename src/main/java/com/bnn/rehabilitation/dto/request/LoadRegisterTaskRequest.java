package com.bnn.rehabilitation.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoadRegisterTaskRequest extends GeneralRequest{
}
