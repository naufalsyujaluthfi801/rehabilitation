package com.bnn.rehabilitation.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadModifyTaskPatientRequest extends GeneralRequest{
    private String id;
}
