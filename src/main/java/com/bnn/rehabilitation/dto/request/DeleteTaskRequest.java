package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

@Data
@EqualsAndHashCode

public class DeleteTaskRequest extends GeneralRequest{
    private String id;
    private String descriptionActivity;
    private MultipartFile file;

    public DeleteTaskRequest() {
    }
}
