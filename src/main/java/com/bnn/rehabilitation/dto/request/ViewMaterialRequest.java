package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode

public class ViewMaterialRequest extends GeneralRequest{
    private String date;
}
