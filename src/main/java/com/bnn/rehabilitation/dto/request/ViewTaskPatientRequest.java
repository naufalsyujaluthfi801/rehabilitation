package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ViewTaskPatientRequest extends GeneralRequest{
    private String date;
    private String id;
}
