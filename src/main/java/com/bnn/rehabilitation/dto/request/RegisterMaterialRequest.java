package com.bnn.rehabilitation.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@EqualsAndHashCode

public class RegisterMaterialRequest extends GeneralRequest {
    private String descriptionMaterial;
    private MultipartFile[] files;
    private List<String> counselorIds;

    public RegisterMaterialRequest() {
    }
}
