package com.bnn.rehabilitation.dto.request;

import com.bnn.rehabilitation.dto.UserSessionDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralRequest {
    private UserSessionDto userSession;
}
