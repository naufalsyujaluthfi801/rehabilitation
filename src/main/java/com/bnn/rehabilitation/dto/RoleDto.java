package com.bnn.rehabilitation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {

    private String id;
    private String code;
    private String name;
    private String type;
    private Boolean isActive;

}