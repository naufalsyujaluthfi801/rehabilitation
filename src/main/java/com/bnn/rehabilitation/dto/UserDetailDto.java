package com.bnn.rehabilitation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailDto {
    private String id;
    private String nik;
    private String fullName;
    private String userName;
    private String password;
    private String dateOfBirth;
    private String placeOfBirth;
    private String address;
    private String gender;
    private String counselor;
}
