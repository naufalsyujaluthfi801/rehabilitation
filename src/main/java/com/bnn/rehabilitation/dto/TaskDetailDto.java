package com.bnn.rehabilitation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDetailDto {
    private String id;
    private String taskId;
    private String uploadId;
    private FileDto fileDto;
    private Boolean isDeleted;
}
