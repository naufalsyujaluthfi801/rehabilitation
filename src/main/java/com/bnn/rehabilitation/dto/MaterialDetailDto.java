package com.bnn.rehabilitation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class MaterialDetailDto {
    private String id;
    private String materialId;
    private String uploadId;
    private FileDto fileDto;
}
