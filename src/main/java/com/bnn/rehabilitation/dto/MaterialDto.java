package com.bnn.rehabilitation.dto;

import com.bnn.rehabilitation.model.MMaterialDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class MaterialDto {
    private String id;
    private String descriptionMaterial;
    private String uploadMaterialId;
    private List<MaterialDetailDto> materialDetailDtos;
}
