package com.bnn.rehabilitation.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

@Data
@NoArgsConstructor
public class FileDto {
    private String mimeType;
    private String filename;
    private File file;
    private String fileId;
}
