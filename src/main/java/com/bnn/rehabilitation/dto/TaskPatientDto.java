package com.bnn.rehabilitation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskPatientDto {
    private String id;
    private String descriptionTask;
    private String uploadTaskId;
    private String fullName;
    private List<TaskPatientDetailDto> taskPatientDetailDtos;
}
