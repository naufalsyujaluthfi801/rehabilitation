package com.bnn.rehabilitation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDto {
    private String id;
    private String startTime;
    private String endTime;
    private String descriptionActivity;
    private Boolean isActive;
    private String fullName;
    private List<ActivityDetailDto> activityDetailDtos;
}
