package com.bnn.rehabilitation.dto.response;

import com.bnn.rehabilitation.dto.GenderDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadModifyUserPatientResponse extends GeneralResponse {
    private List<GenderDto> genderDtoList;
}
