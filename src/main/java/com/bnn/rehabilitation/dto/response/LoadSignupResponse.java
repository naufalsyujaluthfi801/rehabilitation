package com.bnn.rehabilitation.dto.response;

import com.bnn.rehabilitation.dto.RoleDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadSignupResponse extends GeneralResponse {
    private List<RoleDto> roleDtolist;
}
