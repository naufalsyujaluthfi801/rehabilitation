package com.bnn.rehabilitation.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode

public class DeleteTaskResponse extends GeneralResponse{
}
