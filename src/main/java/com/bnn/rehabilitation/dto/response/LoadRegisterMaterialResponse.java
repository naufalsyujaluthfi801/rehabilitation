package com.bnn.rehabilitation.dto.response;

import com.bnn.rehabilitation.dto.RoleDto;
import com.bnn.rehabilitation.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadRegisterMaterialResponse extends GeneralResponse{
    private List<UserDto> userDtoList;

}
