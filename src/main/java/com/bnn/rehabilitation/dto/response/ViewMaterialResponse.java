package com.bnn.rehabilitation.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ViewMaterialResponse extends GeneralResponse {
}
