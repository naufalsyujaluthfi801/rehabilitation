package com.bnn.rehabilitation.dto.response;

import com.bnn.rehabilitation.dto.GenderDto;
import com.bnn.rehabilitation.dto.RoleDto;
import com.bnn.rehabilitation.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadRegisterUserPatientResponse extends GeneralResponse{
    private List<RoleDto> roleDtoList;
    private List<GenderDto> genderDtoList;
    private List<UserDto> userDtoList;

}
