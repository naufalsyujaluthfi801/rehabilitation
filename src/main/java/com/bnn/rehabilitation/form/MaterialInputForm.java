package com.bnn.rehabilitation.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class MaterialInputForm {
    private String id;
    private String descriptionMaterial;
    private MultipartFile[] files;
    private List<String> counselorIds;
}
