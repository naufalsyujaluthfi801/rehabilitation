package com.bnn.rehabilitation.form;

import com.bnn.rehabilitation.dto.ActivityDetailDto;
import com.bnn.rehabilitation.dto.TaskDetailDto;
import com.bnn.rehabilitation.dto.TaskPatientDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class TaskInputForm {
    private String id;
    private String descriptionTask;
    private MultipartFile[] files;
    private List<String> counselorIds;
    private List<TaskPatientDetailDto> taskPatientDetailDtos;
}
