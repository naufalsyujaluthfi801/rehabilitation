package com.bnn.rehabilitation.form;

import lombok.Data;

@Data

public class TaskSearchForm {
    private String date;

    public TaskSearchForm() {
    }
}
