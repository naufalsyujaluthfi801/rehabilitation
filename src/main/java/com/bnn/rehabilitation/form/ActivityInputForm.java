package com.bnn.rehabilitation.form;

import com.bnn.rehabilitation.dto.ActivityDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityInputForm{
    private String id;
    private String startTime;
    private String endTime;
    private String descriptionActivity;
    private MultipartFile[] files;
    private List<ActivityDetailDto> activityDetailDtos;
}
