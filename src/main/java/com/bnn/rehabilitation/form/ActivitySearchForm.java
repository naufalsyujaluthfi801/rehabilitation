package com.bnn.rehabilitation.form;

import lombok.Data;

@Data
public class ActivitySearchForm {
    private String date;

    public ActivitySearchForm() {
    }
}
