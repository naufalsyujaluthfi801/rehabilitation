package com.bnn.rehabilitation.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignupForm {

    private String roleId;
    private String username;
    private String password;
    private String fullName;

}
